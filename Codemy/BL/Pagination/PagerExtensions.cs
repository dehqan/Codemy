using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Codemy.BL.Pagination
{
  public static class PagerExtensions
  {
    /// <summary>
    /// Order the IQueryable by the given property or field.
    /// </summary>
    /// <typeparam name="T">The type of the IQueryable being ordered.</typeparam>
    /// <param name="queryable">The IQueryable being ordered.</param>
    /// <param name="propertyOrFieldName">The name of the property or field to order by.</param>
    /// <param name="ascending"></param>
    /// <returns>Returns an IQueryable ordered by the specified field.</returns>
    public static IQueryable<T> OrderByPropertyOrField<T>(this IQueryable<T> queryable, string propertyOrFieldName,
      bool ascending = true)
    {
      var elementType = typeof(T);
      var orderByMethodName = ascending ? "OrderBy" : "OrderByDescending";

      var parameterExpression = Expression.Parameter(elementType);
      var propertyOrFieldExpression = Expression.PropertyOrField(parameterExpression, propertyOrFieldName);
      var selector = Expression.Lambda(propertyOrFieldExpression, parameterExpression);

      var orderByExpression = Expression.Call(typeof(Queryable), orderByMethodName,
        new[] { elementType, propertyOrFieldExpression.Type }, queryable.Expression, selector);

      return queryable.Provider.CreateQuery<T>(orderByExpression);
    }

    /// <summary>
    /// Creates a paged set of results with AutoMapper.
    /// </summary>
    /// <typeparam name="T">The type of the source IQueryable.</typeparam>
    /// <typeparam name="TReturn">The type of the returned paged results.</typeparam>
    /// <param name="queryable">The source IQueryable.</param>
    /// <returns>Returns a paged set of results.</returns>
    public static async Task<PagedResults<TReturn>> PagedResults<T, TReturn>(
      this IQueryable<T> queryable,
      PageInput model)
    {
      var orderBy = GetDestinationPropertyFor<T, TReturn>(model.OrderByVmProperty);

      var skipAmount = model.PageSize * (model.Page - 1);

      var projection = queryable
        .OrderByPropertyOrField(orderBy, model.IsAscending)
        .Skip(skipAmount)
        .Take(model.PageSize).ProjectTo<TReturn>();

      var totalNumberOfRecords = await queryable.CountAsync();
      var results = await projection.ToListAsync();

      var mod = totalNumberOfRecords % model.PageSize;
      var totalPageCount = (totalNumberOfRecords / model.PageSize) + (mod == 0 ? 0 : 1);



      return new PagedResults<TReturn>
      {
        Results = results,
        PageNumber = model.Page,
        PageSize = results.Count,
        TotalNumberOfPages = totalPageCount,
        TotalNumberOfRecords = totalNumberOfRecords,
        NextPageUrl = ""
      };

    }

    /// <summary>
    /// Creates a paged set of results without AutoMapper.
    /// </summary>
    /// <typeparam name="T">The type of the source IQueryable.</typeparam>
    /// <typeparam name="TReturn">The type of the returned paged results.</typeparam>
    /// <param name="queryable">The source IQueryable.</param>
    /// <returns>Returns a paged set of results.</returns>
    public static PagedResults<TReturn> PagedResults<T, TReturn>(
      this IQueryable<T> queryable,
      Expression<Func<T, TReturn>> selector,
      PageInput model
    )
    {
      if(model.Page <= 0) throw new DataException();
      var orderBy = GetDestinationPropertyFor<T, TReturn>(model.OrderByVmProperty);

      var skipAmount = model.PageSize * (model.Page - 1);

      var results = queryable
        .OrderByPropertyOrField(orderBy, model.IsAscending)
        .Skip(skipAmount)
        .Take(model.PageSize).Select(selector).ToList();

      var totalNumberOfRecords = queryable.Count();

      var mod = totalNumberOfRecords % model.PageSize;
      var totalPageCount = (totalNumberOfRecords / model.PageSize) + (mod == 0 ? 0 : 1);



      return new PagedResults<TReturn>
      {
        Results = results,
        PageNumber = model.Page,
        PageSize = results.Count,
        TotalNumberOfPages = totalPageCount,
        TotalNumberOfRecords = totalNumberOfRecords,
        NextPageUrl = ""
      };

    }


    /// <summary>
    /// Creates a paged set of results with AutoMapper.
    /// </summary>
    /// <typeparam name="T">The type of the source IQueryable.</typeparam>
    /// <typeparam name="TReturn">The type of the returned paged results.</typeparam>
    /// <param name="enumerable">The source IQueryable.</param>
    /// <returns>Returns a paged set of results.</returns>
    public static PagedResults<TReturn> PagedResults<T, TReturn>(
      this IEnumerable<T> enumerable,
      Func<T, TReturn> selector,
      PageInput model
    )
    {
      var orderBy = GetDestinationPropertyFor<T, TReturn>(model.OrderByVmProperty);

      var skipAmount = model.PageSize * (model.Page - 1);
      var propertyInfo = typeof(T).GetProperty(orderBy);
      IOrderedEnumerable<T> orderByProperty = null;
      if (model.IsAscending)
        orderByProperty = enumerable.OrderBy(x => propertyInfo.GetValue(x, null));
      else
      {
        orderByProperty = enumerable.OrderByDescending(x => propertyInfo.GetValue(x, null));

      }

      var results = orderByProperty
        .Skip(skipAmount)
        .Take(model.PageSize).Select(selector).ToList();

      var totalNumberOfRecords = enumerable.Count();

      var mod = totalNumberOfRecords % model.PageSize;
      var totalPageCount = (totalNumberOfRecords / model.PageSize) + (mod == 0 ? 0 : 1);



      return new PagedResults<TReturn>
      {
        Results = results,
        PageNumber = model.Page,
        PageSize = results.Count(),
        TotalNumberOfPages = totalPageCount,
        TotalNumberOfRecords = totalNumberOfRecords,
        NextPageUrl = ""
      };

    }

    public static ServiceProvider ServiceProviderSingleton { get; set; }

    private static string GetDestinationPropertyFor<TSrc, TDst>(string sourceProperty)
    {

      var _mapper = ServiceProviderSingleton.GetService<IMapper>();

      var map = _mapper.ConfigurationProvider.FindTypeMapFor<TSrc, TDst>();
      foreach (var item in map.GetPropertyMaps())
      {
        if (item.DestinationProperty.Name == sourceProperty) return item.SourceMember.Name;
      }
      return "";

    }
  }

}
