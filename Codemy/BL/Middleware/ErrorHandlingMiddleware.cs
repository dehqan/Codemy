using System;
using System.Net;
using System.Threading.Tasks;
using Codemy.API;
using Codemy.Domain.Enum;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace Codemy.BL.Middleware
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate next;

        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await next(context);
            }
            catch (System.Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, System.Exception exception)
        {
            var code = HttpStatusCode.InternalServerError; // 500 if unexpected
            StatusCodeEnum status = StatusCodeEnum.ApplicationError;
            int Hresult = 0;
            bool handled = exception.HResult > 100 && exception.HResult < 1000;

            if (exception is NotImplementedException)
            {
                code = HttpStatusCode.NotImplemented;
                status = StatusCodeEnum.ApplicationError;
            }
            else if (exception is UnauthorizedAccessException)
            {
                code = HttpStatusCode.Unauthorized;
                status = StatusCodeEnum.UserError;

            }
            else if (exception is System.Exception)
            {
                code = HttpStatusCode.BadRequest;
                status = handled ? StatusCodeEnum.UserError : StatusCodeEnum.ApplicationError;
                Hresult = exception.HResult;
            }

            var result = JsonConvert.SerializeObject(
                new CustomResultData
                {
                    Status = status,
                    Error = handled ? exception.Message : exception.ToString(),
                    Result = null
                });
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;
            return context.Response.WriteAsync(result);
        }
    }
}