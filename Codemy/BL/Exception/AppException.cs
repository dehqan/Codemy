﻿namespace Codemy.BL.Exception
{
  public class NameAlreadyExist : System.Exception
  {

    public NameAlreadyExist()
    {
      HResult = 100;
    }

    public override string Message => "نام تکراری است.";
  }


  public class OrderNotExist : System.Exception
  {

    public OrderNotExist()
    {
      HResult = 101;
    }

    public override string Message => "سفارش وجود ندارد.";
  }

  public class OrderExistAndPaid : System.Exception
  {

    public OrderExistAndPaid()
    {
      HResult = 102;
    }

    public override string Message => "سفارش قبلا انجام شده است.";
  }
  public class WorkshopNotFound : System.Exception
  {

    public WorkshopNotFound()
    {
      HResult = 103;
    }

    public override string Message => "کارگاه یافت نشد.";
  }
  public class ProfileNotCompleted : System.Exception
  {

    public ProfileNotCompleted()
    {
      HResult = 104;
    }

    public override string Message => "پروفایل کامل نیست.";
  }
  public class ItemNotFound : System.Exception
  {

    public ItemNotFound()
    {
      HResult = 105;
    }

    public override string Message => "آیتم پیدا نشد.";
  }

  public class FileNotFound : System.Exception
  {

    public FileNotFound()
    {
      HResult = 106;
    }

    public override string Message => "فایل پیدا نشد.";
  }

  public class DownloadLinkNotFound : System.Exception
  {

    public DownloadLinkNotFound()
    {
      HResult = 107;
    }

    public override string Message => "لینک دانلود پیدا نشد.";
  }
    public class PaymentFailed : System.Exception
    {

        private string _message;
        public PaymentFailed(string message)
        {
            HResult = 108;
            _message = message;
        }

        public override string Message => _message;
    }
    public class GeneratedTokenIsNull : System.Exception
    {

        public GeneratedTokenIsNull()
        {
            HResult = 109;
        }

        public override string Message => "توکن نال است.";
    }
    
}
