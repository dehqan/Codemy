using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace Codemy.BL
{
    public static class Extensions
    {
        public static string GetUserId(this ClaimsPrincipal principal)
        {
            if (principal == null)
                throw new ArgumentNullException(nameof(principal));

            var identity = (ClaimsIdentity)principal.Identity;
            var claims = identity.Claims;
            var userId = claims.FirstOrDefault(s => s.Type == "UserId")?.Value;
            if (userId == null) throw new UnauthorizedAccessException();
            return userId;
        }
    }
}
