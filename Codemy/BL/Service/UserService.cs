using System;
using System.Text;
using System.Threading.Tasks;
using Codemy.BL.Service.Interface;
using Codemy.BL.ViewModel.User;
using Codemy.Domain.Entity;
using Codemy.Domain.IRepository;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Codemy.API.Dto.User;

namespace Codemy.BL.Service
{
    public class UserService : IUserService
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly IEmailService _emailService;
        private readonly IContactRepository _contactRepository;
        private readonly IUnitOfWork _unitOfWork;

        public UserService(UserManager<AppUser> userManager,
          IEmailService emailService,
          // IContactRepository contactRepository,
          IUnitOfWork unitOfWork)
        {
            _userManager = userManager;
            _emailService = emailService;
            //_contactRepository = contactRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<GetProfileOutput> GetProfileAsync(string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null) return null;

            return new GetProfileOutput()
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                EmailConfirmed = user.EmailConfirmed,
                Mobile = user.Mobile,
                Avatar = user.Avatar,
                CreateDateTime = user.CreateDateTime,
                Identity = user.Identity,
                IsProfileCompleted = user.IsProfileCompleted

            };
        }

        public async Task<(bool isUpdated, bool isEmailSent)> EditProfileAsync(EditProfileInput model)
        {
            var user = await _userManager.FindByIdAsync(model.UserId);
            if (user == null) return (false, false);

            user.FirstName = model.FirstName;
            user.LastName = model.LastName;

            var emailSend = false;

            if (string.IsNullOrEmpty(user.Email) || user.Email.ToLower() != model.Email.ToLower())
            {
                var fullname = user.FirstName + " " + user.LastName;

                var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                var callbackUrl = "http://www.codemy.ir/Api/Security/ConfirmEmail?userId=" + user.Id + "&token=" + code;

                emailSend = await _emailService.SendEmailVerification(model.Email, fullname, callbackUrl);

                if (emailSend)
                {
                    user.Email = model.Email;
                    user.EmailConfirmed = false;
                }
            }

            var userUpdate = await _userManager.UpdateAsync(user);
            return (userUpdate.Succeeded, emailSend);

        }

        public void sendBuyMessage(string id, string workshopIdentity)
        {
            throw new NotImplementedException();
        }

        // public void AddContact(ContactDto model, string userId)
        // {
        //     _contactRepository.Add(new Contact()
        //     {
        //         Email = model.Email,
        //         Text = model.Text,
        //         Title = model.Title,
        //         UserId = userId
        //     });

        //     _unitOfWork.Save();

        // }
    }
}
