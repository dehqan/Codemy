using System;
using System.Collections.Generic;
using Codemy.BL.Exception;
using Codemy.BL.Service.Interface;
using Codemy.Domain.Entity;
using Codemy.Domain.Enum;
using Codemy.Domain.IRepository;
using SmsIrRestful;

namespace Codemy.BL.Service
{
    public class SmsService : ISmsService
    {
        private readonly IUnitOfWork _unitOfWork;
        public SmsService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public bool SendVerificationCode(string userId, string mobile, string verificationCode)
        {
            var token = new Token().GetToken("ac771d3fd77e79a6de804708", "P@ssw0rd");
            if (token == null) throw new GeneratedTokenIsNull();
            var ultraFastSend = new UltraFastSend()
            {
                Mobile = Convert.ToInt64(mobile),
                TemplateId = 1344,
                ParameterArray = new List<UltraFastParameters>()
                    {
                        new UltraFastParameters()
                        {
                            Parameter = "VerificationCode" , ParameterValue = verificationCode
                        }
                    }.ToArray()
            };

            UltraFastSendRespone ultraFastSendRespone = new UltraFast().Send(token, ultraFastSend);

            var smsLog = new SmsLog
            {
                UserId = userId,
                Mobile = mobile,
                Content = verificationCode,
                DeliveryMessage = ultraFastSendRespone.Message,
                Type = SmsTypeEnum.Register,
                Status = ultraFastSendRespone.IsSuccessful ? SmsStatusEnum.Delivered : SmsStatusEnum.Failed
            };
            _unitOfWork.SmsLogRepository.Add(smsLog);
            _unitOfWork.Save();

            return ultraFastSendRespone.IsSuccessful;
        }
    }
}