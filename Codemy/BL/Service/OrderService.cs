using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Codemy.BL.Exception;
using Codemy.BL.Pagination;
using Codemy.BL.Service.Interface;
using Codemy.BL.ViewModel.Order;
using Codemy.BL.ViewModel.Post;
using Codemy.Domain.Entity;
using Codemy.Domain.Enum;
using Codemy.Domain.IRepository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Codemy.BL.Service
{

    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserService _userService;
        private readonly BankConfiguration _bankConfiguration;

        public OrderService(IUnitOfWork unitOfWork, IUserService userService,
            IOptions<BankConfiguration> bankConfiguration)
        {
            _unitOfWork = unitOfWork;
            _userService = userService;
            _bankConfiguration = bankConfiguration.Value;
        }

        public async Task<(BuyWorkshopOutput order, string bankUrl)> BuyWorkshop(BuyWorkshopInput model)
        {
            var profile = await _userService.GetProfileAsync(model.UserId);
            if (profile.IsProfileCompleted != true)
            {
                throw new ProfileNotCompleted();
            }

            var workshop = await _unitOfWork.WorkshopRepository.FindAsync(x => x.Identity == model.WorkshopIdentity);
            if (workshop == null)
            {
                throw new WorkshopNotFound();
            }

            var order = await GetOrder(workshop.Id.ToString(), model.UserId);
            if (order != null && order.Status == OrderStatusEnum.Paid)
            {
                throw new OrderExistAndPaid();
            }


            if (order == null)
            {
                order = new Order()
                {
                    UserId = model.UserId,
                    Status = workshop.Cost == 0 ? OrderStatusEnum.Paid : OrderStatusEnum.NotPaid,
                    WorkshopId = workshop.Id,
                    TransactionId = ""
                };
                _unitOfWork.OrderRepository.Add(order);
                _unitOfWork.Save();
            }


            var bankDetail = _bankConfiguration;
            if (workshop.Cost != 0)
            {
                if (bankDetail.Name.ToLower() == "zarinpaltest")
                {
                    var payment = new ZarinpalSandbox.Payment(workshop.Cost);
                    var paymentRequestResult = await payment.PaymentRequest($"ثبت نام کارگاه{workshop.Title}", bankDetail.CallBackUrl, profile.Email ?? "", profile.Mobile);
                    order.PaymentRequestOutputStatus = paymentRequestResult.Status;
                    order.PaymentRequestOutputAuthority = paymentRequestResult.Authority;
                    order.PaymentRequestAsyncDescription = bankDetail.BankDescriptions
                      .FirstOrDefault(x => x.Key == paymentRequestResult.Status.ToString())?.Value;

                    _unitOfWork.Save();
                    if (paymentRequestResult.Status != 100)
                    {
                        throw new PaymentFailed(_bankConfiguration.BankDescriptions.FirstOrDefault(x => x.Key == paymentRequestResult.Status.ToString()).Value);
                    }
                    return (new BuyWorkshopOutput {
                        WorkshopIdentity = order.WorkshopId,
                        Identity=order.Identity,
                        PaymentRequestAsyncDescription = order.PaymentRequestAsyncDescription,
                        Status=order.Status
                    }, bankDetail.BankUrl + order.PaymentRequestOutputAuthority);

                }
                else
                {
                    var payment = new Zarinpal.Payment(bankDetail.MerchantID, workshop.Cost);
                    var paymentRequestResult = await payment.PaymentRequest($"ثبت نام کارگاه{workshop.Title}", bankDetail.CallBackUrl, profile.Email ?? "", profile.Mobile);
                    order.PaymentRequestOutputStatus = paymentRequestResult.Status;
                    order.PaymentRequestOutputAuthority = paymentRequestResult.Authority;
                    order.PaymentRequestAsyncDescription = bankDetail.BankDescriptions
                      .FirstOrDefault(x => x.Key == paymentRequestResult.Status.ToString())?.Value;

                    _unitOfWork.Save();
                    if (paymentRequestResult.Status != 100)
                    {
                        throw new PaymentFailed(_bankConfiguration.BankDescriptions.FirstOrDefault(x => x.Key == paymentRequestResult.Status.ToString()).Value);
                    }
                    return (new BuyWorkshopOutput
                    {
                        WorkshopIdentity = order.WorkshopId,
                        Identity = order.Identity,

                        PaymentRequestAsyncDescription = order.PaymentRequestAsyncDescription,
                        Status = order.Status
                    }, bankDetail.BankUrl + order.PaymentRequestOutputAuthority);

                }

            }

            return (new BuyWorkshopOutput
            {
                WorkshopIdentity = order.WorkshopId,
                Identity = order.Identity,

                PaymentRequestAsyncDescription = order.PaymentRequestAsyncDescription,
                Status = order.Status
            }, "");



        }

        public async Task<BuyWorkshopOutput> RedirectFromBank(string authority, string status)
        {

            var registeredOrder = await _unitOfWork.OrderRepository.AsQueryable().Include(x => x.Workshop)
              .FirstOrDefaultAsync(x => x.PaymentRequestOutputAuthority == authority);
            if (registeredOrder == null) throw new OrderNotExist();

            var bankDetail = _bankConfiguration;
            if (bankDetail.Name.ToLower() == "zarinpaltest")
            {
                var payment = new ZarinpalSandbox.Payment(registeredOrder.Workshop.Cost);
                var result = await payment.Verification(authority);

                registeredOrder.Status = (result.Status == 100 || result.Status == 101) ? OrderStatusEnum.Paid : OrderStatusEnum.NotPaid;

                registeredOrder.TransactionId = result.RefId.ToString();
                registeredOrder.PaymentVerificationAsyncDescription = bankDetail.BankDescriptions
                  .FirstOrDefault(x => x.Key == result.Status.ToString())?.Value;

                _unitOfWork.Save();


                return (new BuyWorkshopOutput
                {
                    WorkshopIdentity = registeredOrder.Workshop.Identity.ToString(),

                    Identity = registeredOrder.Identity,

                    PaymentRequestAsyncDescription = registeredOrder.PaymentRequestAsyncDescription,
                    Status = registeredOrder.Status
                });
            }
            else

            {
                var payment = new Zarinpal.Payment(bankDetail.MerchantID, registeredOrder.Workshop.Cost);
                var result = await payment.Verification(authority);

                registeredOrder.Status = result.Status == 100 ? OrderStatusEnum.Paid : OrderStatusEnum.NotPaid;


                registeredOrder.TransactionId = result.RefId.ToString();
                registeredOrder.PaymentVerificationAsyncDescription = bankDetail.BankDescriptions
                  .FirstOrDefault(x => x.Key == result.Status.ToString())?.Value;

                _unitOfWork.Save();


                return (new BuyWorkshopOutput
                {
                    WorkshopIdentity = registeredOrder.Workshop.Identity.ToString(),
                    Identity = registeredOrder.Identity,

                    PaymentRequestAsyncDescription = registeredOrder.PaymentRequestAsyncDescription,
                    Status = registeredOrder.Status
                });
            }
        }

        public PagedResults<GetRegisteredWorkshopListItemOutput> GetRegisteredWorkshopList(PageInput page, string userId)
        {
            return _unitOfWork.OrderRepository
              .AsQueryable()
              .Include(x => x.Workshop)
              .Where(x => x.UserId == userId)
              .AsQueryable()
              .PagedResults(x => new GetRegisteredWorkshopListItemOutput
              {
                  Id = x.Id,
                  Identity = x.Identity,
                  CreateDateTime = x.CreateDateTime,
                  Title = x.Workshop.Title,
                  Cost = x.Workshop.Cost,
                  Status = x.Status,
                  HoldingDateTime = x.Workshop.HoldingDateTime

              }, page);
        }

        public async Task<GetRegisteredWorkshopDetailOutput> GetRegisteredWorkshopDetail(string orderId)
        {
            var item = await _unitOfWork.OrderRepository.AsQueryable().Include(x => x.Workshop)
              .FirstOrDefaultAsync(x => x.Id == orderId);

            return new GetRegisteredWorkshopDetailOutput()
            {
                Id = item.Id,
                CreateDateTime = item.CreateDateTime,
                Identity = item.Identity,
                IsPaid = item.Status == OrderStatusEnum.Paid,
                Description = item.Description,
                TransId = item.TransactionId,
                Duration = item.Workshop.Duration,
                Price = item.Workshop.Cost,
                Title = item.Workshop.Title,
                Rate = item.Workshop.Rate
            };
        }

        public async Task<bool> IsBought(string userId, string workshopId)
        {
            return await _unitOfWork.OrderRepository.AsQueryable().AnyAsync(x =>
              x.WorkshopId == workshopId && x.UserId == userId && x.Status == OrderStatusEnum.Paid);
        }


        private async Task<Order> GetOrder(string workshopId, string userId)
        {
            return await _unitOfWork.OrderRepository.FindAsync(x =>
              x.WorkshopId == workshopId && x.UserId == userId && x.Status == OrderStatusEnum.Paid);
        }

        public async Task<List<string>> GetRegisteredWorkshopIdsAsync(string userId)
        {
            return await _unitOfWork.OrderRepository.AsQueryable().Where(x =>
              x.UserId == userId && x.Status == OrderStatusEnum.Paid).Select(x=> x.WorkshopId).ToListAsync();
        }
    }
}
