using System.IO;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Codemy.BL.Service.Interface;
using Codemy.Domain.IRepository;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace Codemy.BL.Service
{
    public class EmailService : IEmailService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IHostingEnvironment _environment;
        private readonly IConfiguration _configuration;
        public EmailService(IUnitOfWork unitOfWork, IHostingEnvironment environment, IConfiguration configuration)
        {
            _unitOfWork = unitOfWork;
            _environment = environment;
            _configuration = configuration;
        }

        public async Task<bool> SendEmailVerification(string to, string fullName, string callbackUrl)
        {
            var subject = "تایید ایمیل";
            var body = string.Empty;
            var template = _environment.ContentRootPath + "\\Resources\\Email\\EmailVerificationTemplate.html";

            using (StreamReader reader = new StreamReader(template))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{{Fullname}}", fullName);
            body = body.Replace("{{Subject}}", subject);
            body = body.Replace("{{CallbackUrl}}", callbackUrl);

            return await Send(to, subject, body);
        }

        private async Task<bool> Send(string to, string subject, string body)
        {
            try
            {
                var smtpServer = _configuration["SmtpServer"];
                var emailNoReply = _configuration["EmailNoReply"];
                var emailPasswordNoReply = _configuration["EmailPasswordNoReply"];

                SmtpClient client = new SmtpClient(smtpServer);
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(emailNoReply, emailPasswordNoReply);

                MailMessage message = new MailMessage();
                message.From = new MailAddress(emailNoReply);
                message.To.Add(to);
                message.Body = body;
                message.Subject = subject;
                client.SendCompleted += (s, e) =>
                {
                    client.Dispose();
                    message.Dispose();

                };
                await client.SendMailAsync(message);
                return true;
            }
            catch (System.Exception exception)
            {
                return false;
            }

        }
    }
}