using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Codemy.BL.Exception;
using Codemy.BL.Pagination;
using Codemy.BL.Service.Interface;
using Codemy.BL.ViewModel.Workshop;
using Codemy.Domain.Entity;
using Codemy.Domain.Enum;
using Codemy.Domain.IRepository;
using Microsoft.EntityFrameworkCore;

namespace Codemy.BL.Service
{
    public class WorkshopService : IWorkshopService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IOrderService _orderService;

        public WorkshopService(IUnitOfWork unitOfWork, IOrderService orderService)
        {
            _unitOfWork = unitOfWork;
            _orderService = orderService;
        }

        private void UpdateWorkshopsRate()
        {
            var workshops = _unitOfWork.WorkshopRepository.GetAll();
            foreach (var workshop in workshops)
            {
                //get last rate of each user
                var rates = _unitOfWork.WorkshopRateRepository.AsQueryable()
                .GroupBy(pm => pm.UserId)
                .SelectMany(g => g.OrderByDescending(pm => pm.CreateDateTime)
                .Where(x => x.WorkshopId == workshop.Id)
                .Take(1));

                //sum & count
                var sum = rates.Sum(x => x.Rate);
                var count = rates.Count();

                //DivideByZeroException
                if (count == 0) continue;

                //update workshop table
                workshop.RateSum = sum;
                workshop.RateCount = count;
                workshop.Rate = sum / count;

                _unitOfWork.Save();
            }
        }

        public PagedResults<GetWorkshopListOutput> GetWorkshopList(PageInput page, WorkshopStatusEnum status = WorkshopStatusEnum.None)
        {
            UpdateWorkshopsRate();

            return _unitOfWork.WorkshopRepository
                 .AsQueryable()
                 .Where(x => status == WorkshopStatusEnum.None || x.Status == status)
                 .PagedResults(x => new GetWorkshopListOutput
                 {
                     Id = x.Id,
                     Identity = x.Identity,
                     CreateDateTime = x.CreateDateTime,
                     Title = x.Title,
                     UrlTitle = x.UrlTitle,
                     Duration = x.Duration,
                     ImageSrc = x.ImageSrc,
                     _holdingDateTime = x.HoldingDateTime,
                     Cost = x.Cost,
                     Rate = x.Rate,
                     Status = x.Status,
                     Course = x.Course.Title,
                     CourseId = x.Course.Identity,
                     UserDetail = x.UserDetail.UserName,
                     TeacherId = x.TeacherId

                 }, page);
        }

        public GetWorkshopOutput GetWorkshop(long workshopId)
        {
            UpdateWorkshopsRate();

            return _unitOfWork.WorkshopRepository
          .AsQueryable()
          .Where(x => x.Identity == workshopId)
          .Select(x => new GetWorkshopOutput
          {
              Id = x.Id,
              Identity = x.Identity,
              CreateDateTime = x.CreateDateTime,
              Title = x.Title,
              Duration = x.Duration,
              ImageSrc = x.ImageSrc,
              _holdingDateTime = x.HoldingDateTime,
              Cost = x.Cost,
              Rate = x.Rate.ToString(),
              Status = x.Status,
              Course = x.Course.Title,
              CourseId = x.Course.Identity,
              TeacherName = x.UserDetail.FirstName +" "+ x.UserDetail.LastName,
              TeacherDescription = x.UserDetail.Description,
              TeacherId = x.TeacherId,
              Description = x.Description,
              Goal = x.Goal,
              PreRequirements = x.PreRequirements

          }).FirstOrDefault();
        }

        public GetWorkshopSummaryOutput GetWorkshopSummary(string workshopId)
        {
            return _unitOfWork.WorkshopRepository
          .AsQueryable()
          .Where(x => x.Id == workshopId)
          .Select(x => new GetWorkshopSummaryOutput
          {
              Id = x.Id,
              Identity = x.Identity,
              CreateDateTime = x.CreateDateTime,
              Title = x.Title,
              ImageSrc = x.ImageSrc,
              _holdingDateTime = x.HoldingDateTime,
              Cost = x.Cost,
              Status = x.Status,
              UserDetail = x.UserDetail.UserName,
              TeacherId = x.TeacherId

          }).FirstOrDefault();
        }

        public PagedResults<GetRelatedWorkshopListOutput> GetRelatedWorkshopList(string workshopId, string categoryId, PageInput page)
        {
            return _unitOfWork.WorkshopRepository
              .AsQueryable()
              .Where(x => x.Id != workshopId && x.Course.CategoryId == categoryId)
              .PagedResults(x => new GetRelatedWorkshopListOutput
              {
                  Id = x.Id,
                  Identity = x.Identity,
                  CreateDateTime = x.CreateDateTime,
                  Title = x.Title,
                  ImageSrc = x.ImageSrc,
                  _holdingDateTime = x.HoldingDateTime,
                  Cost = x.Cost,
                  Status = x.Status,
                  UserDetail = x.UserDetail.UserName,
                  TeacherId = x.TeacherId
              }, page);
        }

        public PagedResults<GetWorkshopCommentListOutput> GetWorkshopCommentList(string workshopId, PageInput page)
        {
            return _unitOfWork.WorkshopCommentRepository
            .AsQueryable()
            .Where(x => x.WorkshopId == workshopId && x.ParentCommentId == null)
            .PagedResults(x => new GetWorkshopCommentListOutput
            {
                Id = x.Id,
               Identity = x.Identity,
                CreateDateTime = x.CreateDateTime,
                Text = x.Text,
                UserDetail = x.UserDetail.UserName,
                UserId = x.UserId,
                Workshop = x.Workshop.Title,
                WorkshopId = x.Workshop.Identity,
                Children = x.Replays.Select(y => new GetWorkshopCommentListOutput
                {
                    Id = y.Id,
                    Identity = y.Identity,
                    CreateDateTime = y.CreateDateTime,
                    Text = y.Text,
                    UserDetail = y.UserDetail.UserName,
                    UserId = y.UserId,
                    Workshop = y.Workshop.Title,
                    WorkshopId = y.Workshop.Identity,
                }).OrderBy(z => z.CreateDateTime).ToList()
            }, page);
        }

        public PagedResults<GetCourseWorkshopListOutput> GetCourseWorkshopList(string courseId, PageInput page)
        {
            UpdateWorkshopsRate();

            return _unitOfWork.WorkshopRepository
              .AsQueryable()
              .Where(x => x.Course.Id == courseId)
              .PagedResults(x => new GetCourseWorkshopListOutput
              {
                  Id = x.Id,
                  Identity = x.Identity,
                  CreateDateTime = x.CreateDateTime,
                  Title = x.Title,
                  Duration = x.Duration,
                  ImageSrc = x.ImageSrc,
                  _holdingDateTime = x.HoldingDateTime,
                  Cost = x.Cost,
                  Rate = x.Rate,
                  Status = x.Status,
                  Course = x.Course.Title,
                  CourseId = x.Course.Identity,
                  UserDetail = x.UserDetail.UserName,
                  TeacherId = x.TeacherId

              }, page);
        }

        public GetWorkshopCommentListOutput AddWorkshopComment(AddWorkshopCommentInput model)
        {
            var workshopComment = new WorkshopComment
            {
                UserId = model.UserId,
                WorkshopId = model.WorkshopId,
                Text = model.Text,
            };

            _unitOfWork.WorkshopCommentRepository.Add(workshopComment);
            _unitOfWork.Save();

            return new GetWorkshopCommentListOutput()
            {
                Id = workshopComment.Id,
                Identity = workshopComment.Identity,
                CreateDateTime = workshopComment.CreateDateTime,
                Text = workshopComment.Text


            };
        }

        public void AddWorkshopRate(AddWorkshopRateInput model)
        {
            var workshopRate = new WorkshopRate
            {
                UserId = model.UserId,
                WorkshopId = model.WorkshopId,
                Rate = model.Rate,
            };

            _unitOfWork.WorkshopRateRepository.Add(workshopRate);
            _unitOfWork.Save();
        }

        public async Task<string> DownloadworkshopFileId(string userId, string workshopFileId)
        {
            var item = await _unitOfWork.WorkshopFileRepository.FindAsync(x => x.Id == workshopFileId);
            if (item == null) throw new FileNotFound();
            var isBought = await _orderService.IsBought(userId, item.WorkshopId);
            if (!isBought) throw new OrderNotExist();

            var downloadLink = await _unitOfWork.DownloadLinkRepository.FindAsync(x =>
                x.UserId == userId && x.WorkshopFileId == workshopFileId);


            if (downloadLink == null)
            {
                _unitOfWork.DownloadLinkRepository.Add(new DownloadLink()
                {
                    DownloadCount = 1,
                    UserId = userId,
                    WorkshopFileId = workshopFileId,
                });
            }
            else
            {
                downloadLink.DownloadCount++;
            }
            _unitOfWork.Save();

            return item.FileSrc;

        }

        public PagedResults<GetCourseListItemOutput> GetCourseList(PageInput page)
        {
            return _unitOfWork.CourseRepository
              .AsQueryable()
              .Include(x => x.Category)
              .AsQueryable()
              .PagedResults(x => new GetCourseListItemOutput
              {
                  Id = x.Id,
                  Identity = x.Identity,
                  CreateDateTime = x.CreateDateTime,
                  Title = x.Title,
                  Description = x.Description,
                  Category = x.Category.Title,
                  ImageSrc = x.ImageSrc,
                  UrlTitle = x.UrlTitle
              }, page);
        }

        public GetCourseOutput GetCourse(long id)
        {
            return _unitOfWork.CourseRepository.AsQueryable().Include(x => x.Category)
              .Where(x => x.Identity == id).Include(x => x.CourseTags)
              .ThenInclude(x => x.Tag)
              .Select(x => new GetCourseOutput()
              {
                  Id = x.Id,
                  Identity = x.Identity,
                  CreateDateTime = x.CreateDateTime,
                  Category = new Category_GetCourseOutput() { Title = x.Category.Title, UrlTitle = x.Category.UrlTitle },
                  Title = x.Title,
                  Description = x.Description,
                  ImageSrc = x.ImageSrc,
                  Tags = x.CourseTags.Select(y => y.Tag).Select(y => new Tag_GetCourseOutput() { Title = y.Title, UrlTitle = y.UrlTitle }).ToList()
              }).FirstOrDefault();
        }

        public Task<bool> hasCapacity(long id)
        {
            throw new NotImplementedException();
        }

        public Task<bool> isExpired(long id)
        {
            throw new NotImplementedException();
        }
    }
}
