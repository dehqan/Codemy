using System.Linq;
using Codemy.BL.Pagination;
using Codemy.BL.Service.Interface;
using Codemy.BL.ViewModel.Post;
using Codemy.Domain.Enum;
using Codemy.Domain.IRepository;

namespace Codemy.BL.Service
{
  public class CategoryService : ICategoryService
  {
    private readonly IUnitOfWork _unitOfWork;

    public CategoryService(IUnitOfWork unitOfWork)
    {
      _unitOfWork = unitOfWork;

    }


    public PagedResults<GetCategoryListOutput> GetCategoryList(PageInput page, CategoryTypeEnum categoryType)
    {
      return _unitOfWork.CategoryRepository
        .AsQueryable()
        .Where(x => x.Type == categoryType)
        .AsQueryable()
        .PagedResults(x => new GetCategoryListOutput
        {
          Id = x.Id,
          Identity = x.Identity,
          CreateDateTime = x.CreateDateTime,
          Title = x.Title,
        }, page);
    }

  }
}
