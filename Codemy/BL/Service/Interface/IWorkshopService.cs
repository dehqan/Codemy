using System.Collections.Generic;
using System.Threading.Tasks;
using Codemy.BL.Pagination;
using Codemy.BL.ViewModel.Workshop;
using Codemy.Domain.Enum;

namespace Codemy.BL.Service.Interface
{
    public interface IWorkshopService
    {
        PagedResults<GetWorkshopListOutput> GetWorkshopList(PageInput page, WorkshopStatusEnum status = WorkshopStatusEnum.None);

        GetWorkshopOutput GetWorkshop(long workshopId);

        GetWorkshopSummaryOutput GetWorkshopSummary(string workshopId);

        PagedResults<GetRelatedWorkshopListOutput> GetRelatedWorkshopList(string workshopId, string categoryId, PageInput page);

        PagedResults<GetWorkshopCommentListOutput> GetWorkshopCommentList(string workshopId, PageInput page);

        PagedResults<GetCourseWorkshopListOutput> GetCourseWorkshopList(string courseId, PageInput page);

        GetWorkshopCommentListOutput AddWorkshopComment(AddWorkshopCommentInput model);

        void AddWorkshopRate(AddWorkshopRateInput model);
        Task<string> DownloadworkshopFileId(string userId, string workshopFileId);
        PagedResults<GetCourseListItemOutput> GetCourseList(PageInput page);
        GetCourseOutput GetCourse(long id);
        Task<bool> hasCapacity(long id);
        Task<bool> isExpired(long id);
    }
}
