using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Codemy.BL.Pagination;
using Codemy.BL.ViewModel.Post;
using Codemy.Domain.Entity;
using Codemy.Domain.Enum;

namespace Codemy.BL.Service.Interface
{
  public interface IOrderService
  {
    Task<(BuyWorkshopOutput order, string bankUrl)> BuyWorkshop(BuyWorkshopInput model);


    Task<BuyWorkshopOutput> RedirectFromBank(string authority, string status);
    //Task<Order> RedirectFromBank(string authority, string status);

    PagedResults<GetRegisteredWorkshopListItemOutput> GetRegisteredWorkshopList(PageInput page, string userId);
    Task<GetRegisteredWorkshopDetailOutput> GetRegisteredWorkshopDetail(string orderId);

    Task<bool> IsBought(string userId, string itemWorkshopId);

    Task<List<string>> GetRegisteredWorkshopIdsAsync(string userId);
    }
}
