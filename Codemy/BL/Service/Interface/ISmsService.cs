namespace Codemy.BL.Service.Interface
{
    public interface ISmsService
    {
        bool SendVerificationCode(string userId, string mobile, string verificationCode);
    }
}