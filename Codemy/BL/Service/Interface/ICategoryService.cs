using Codemy.BL.Pagination;
using Codemy.BL.ViewModel.Post;
using Codemy.Domain.Enum;

namespace Codemy.BL.Service.Interface
{
  public interface ICategoryService
  {
    PagedResults<GetCategoryListOutput> GetCategoryList(PageInput page, CategoryTypeEnum categoryType);

  }
}
