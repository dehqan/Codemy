using Codemy.BL.Pagination;
using Codemy.BL.ViewModel.Post;
using Codemy.Domain.Entity;
using Codemy.Domain.Enum;

namespace Codemy.BL.Service.Interface
{
  public interface IPostService
  {
    PagedResults<GetPostListItemOutput> GetPostList(PageInput page, PostTypeEnum postType, string categoryUrlTitle, bool JustSpecial = false);
    PagedResults<GetPostListItemOutput> GetRelatedPostList(PageInput page, string id, string categoryId);
    PagedResults<GetPostCommentListItemOutput> GetPostCommentList(string postId, PageInput page);
    GetPostOutput GetPost(long id);
    GetPostCommentListItemOutput AddPostComment(AddPostComment model);
  }
}
