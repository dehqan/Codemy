using System;
using System.Threading.Tasks;
using Codemy.API.Dto.User;
using Codemy.BL.ViewModel.User;

namespace Codemy.BL.Service.Interface
{
    public interface IUserService
    {
        Task<GetProfileOutput> GetProfileAsync(string userId);
        Task<(bool isUpdated, bool isEmailSent)> EditProfileAsync(EditProfileInput model);
        void sendBuyMessage(string id, string workshopIdentity);
        //   oid AddContact(ContactDto model, string userId);


    }
}
