using System.Threading.Tasks;

namespace Codemy.BL.Service.Interface
{
    public interface IEmailService
    {
        Task<bool> SendEmailVerification(string to, string fullName, string callbackUrl);
    }
}