﻿using System.Linq;
using Codemy.BL.Pagination;
using Codemy.BL.Service.Interface;
using Codemy.BL.ViewModel.Post;
using Codemy.Domain.Entity;
using Codemy.Domain.Enum;
using Codemy.Domain.IRepository;
using Microsoft.EntityFrameworkCore;

namespace Codemy.BL.Service
{

  public class PostService : IPostService
  {
    private readonly IUnitOfWork _unitOfWork;

    public PostService(IUnitOfWork unitOfWork)
    {
      _unitOfWork = unitOfWork;

    }

    public PagedResults<GetPostListItemOutput> GetPostList(PageInput page,
      PostTypeEnum postType,
      string categoryUrlTitle,
      bool JustSpecial = false

      )
    {
      return _unitOfWork.PostRepository
        .AsQueryable()
        .Include(x => x.Category)
        .Where(x => (postType == PostTypeEnum.None || x.PostType == postType)
        &&
        (string.IsNullOrEmpty(categoryUrlTitle) || x.UrlTitle == categoryUrlTitle)
        &&
        (!JustSpecial || x.IsSpecial == true)
        )
        .AsQueryable()
        .PagedResults(x => new GetPostListItemOutput
        {
          Id = x.Id,
          Identity = x.Identity,
          CreateDateTime = x.CreateDateTime,
          Title = x.Title,
          Summary = x.Summary,
          UrlTitle = x.UrlTitle,
          Image = x.Image,
          Category = x.Category.Title
        }, page);
    }


    public PagedResults<GetPostListItemOutput> GetRelatedPostList(PageInput page,
      string id,
      string categoryId
    )
    {
      return _unitOfWork.PostRepository
        .AsQueryable()
        .Include(x => x.Category)
        .Where(x => x.Id != id && x.CategoryId == categoryId
        )
        .AsQueryable()
        .PagedResults(x => new GetPostListItemOutput
        {
          Id = x.Id,
          Identity = x.Identity,
          CreateDateTime = x.CreateDateTime,
          Title = x.Title,
          Summary = x.Summary,
          UrlTitle = x.UrlTitle,
          Category = x.Category.Title
        }, page);
    }
    public GetPostOutput GetPost(long id)
    {
      return _unitOfWork.PostRepository.AsQueryable().Include(x => x.Category)
        .Where(x => x.Identity == id).Include(x => x.PostTags)
        .ThenInclude(x => x.Tag)
        .Select(x => new GetPostOutput()
        {
          Id = x.Id,
          Identity = x.Identity,
          CreateDateTime = x.CreateDateTime,
          Category = new Category_GetPostOutput() { Title = x.Category.Title, UrlTitle = x.Category.UrlTitle },
          UrlTitle = x.UrlTitle,
          Title = x.Title,
          PostContent = x.PostContent,
          PostType = x.PostType,
          Summary = x.Summary,
          Image=x.Image,
          Tags = x.PostTags.Select(y => y.Tag).Select(y => new Tag_GetPostOutput() { Title = y.Title, UrlTitle = y.UrlTitle }).ToList()
        }).FirstOrDefault();
    }

    public GetPostCommentListItemOutput AddPostComment(AddPostComment model)
    {

      //چک کن اگر یک چیلدرن قرار است جواب داده شود
      //در حقیقت باید خود کامنت جواب داده شود.
      //یعنی پدرش 
      //--comment+
      //---------commentchild*
      //---------commentchild-
      //مثلا برای پاسخ به ستاره فوق باید جواب در زیر آن قرار گیرد پس آی دی ستاره
      //نباید پرنت کامنت ما بشود
      //پرنت ما مثبت خواهد بود و کامنت نوشته شده در محل منفی قرار خواهد گرفت
      var parentComment = _unitOfWork.PostCommentRepository.AsQueryable().FirstOrDefault(x => x.Id == model.ParentId);
      if (parentComment != null)
      {
        model.ParentId = parentComment.Id;
      }

      var postComment = new PostComment()
      {
        UserId = model.UserId,
        PostId = model.PostId,
        Text = model.Text,
        ParentId = model.ParentId
      };

      _unitOfWork.PostCommentRepository.Add(postComment);
      _unitOfWork.Save();

      return new GetPostCommentListItemOutput()
      {
        Id = postComment.Id,
        Identity = postComment.Identity,
        CreateDateTime = postComment.CreateDateTime,
        Text = postComment.Text


      };
    }

    public PagedResults<GetPostCommentListItemOutput> GetPostCommentList(string postId, PageInput page)
    {
      return _unitOfWork.PostCommentRepository
        .AsQueryable().Include(x => x.UserDetail)
        .Where(x => x.PostId == postId && x.ParentId == null)
        .PagedResults(x => new GetPostCommentListItemOutput
        {
          Id = x.Id,
          Identity = x.Identity,
          CreateDateTime = x.CreateDateTime,
          Text = x.Text,
          Rate = x.Rate,
          Username = x.UserDetail.UserName,
          Children = _unitOfWork.PostCommentRepository
            .AsQueryable().Include(y => y.UserDetail)
            .Where(y => x.Id == y.ParentId).Select(y => new GetPostCommentListItemOutput
            {
              Id = y.Id,
              Identity = y.Identity,
              CreateDateTime = y.CreateDateTime,
              Text = y.Text,
              Rate = y.Rate,
              Username = y.UserDetail.UserName
            }).OrderBy(z => z.CreateDateTime).ToList()
        }, page);
    }


  }
}
