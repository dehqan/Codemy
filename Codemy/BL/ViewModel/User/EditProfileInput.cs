namespace Codemy.BL.ViewModel.User
{
    public class EditProfileInput
    {
        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Avatar { get; set; }
        public string Email { get; set; }
    }
}