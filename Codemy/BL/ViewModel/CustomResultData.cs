using Codemy.Domain.Enum;

namespace Codemy.BL
{
    public class CustomResultData
    {
        public StatusCodeEnum Status { get; set; }
        public string Error { get; set; }
        public object Result { get; set; }
    }
}
