using System.Collections.Generic;

namespace Codemy.BL.ViewModel.Workshop
{
    public class GetWorkshopCommentListOutput : BaseVm
    {
        public string Text { get; set; }
        public string UserDetail { get; set; }
        public string UserId { get; set; }
        public string Workshop { get; set; }
        public long WorkshopId { get; set; }
        public List<GetWorkshopCommentListOutput> Children { get; set; }
    }
}