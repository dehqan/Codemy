﻿using System;
using Codemy.Domain.Enum;

namespace Codemy.BL.ViewModel.Workshop
{
    public class GetWorkshopListOutput : BaseVm
    {
        public string Title { get; set; }
        public string UrlTitle { get; set; }

        public int Duration { get; set; }
        public string ImageSrc { get; set; }
        public DateTime _holdingDateTime { private get; set; }
        public string HoldingDateTime => _holdingDateTime.GetHashCode() == 0 ? "" : _holdingDateTime.ToShamsiDateTime();

        public int Cost { get; set; }
        public decimal Rate { get; set; }
        public WorkshopStatusEnum Status { get; set; }
        public string Course { get; set; }
        public long CourseId { get; set; }
        public string UserDetail { get; set; }
        public string TeacherId { get; set; }
    }
}
