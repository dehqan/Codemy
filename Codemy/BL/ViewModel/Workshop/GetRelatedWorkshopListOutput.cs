﻿using System;
using Codemy.Domain.Enum;

namespace Codemy.BL.ViewModel.Workshop
{
  public class GetRelatedWorkshopListOutput : BaseVm
  {
    public string Title { get; set; }
    public string ImageSrc { get; set; }    
    public DateTime _holdingDateTime { private get; set; }    
    public string HoldingDateTime => _holdingDateTime.ToShamsiDateTime();  
    public int Cost { get; set; }
    public WorkshopStatusEnum Status { get; set; }
    public string UserDetail { get; set; }
    public string TeacherId { get; set; }
  }
}
