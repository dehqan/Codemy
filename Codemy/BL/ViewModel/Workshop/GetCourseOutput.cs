using System.Collections.Generic;
using Codemy.Domain.Enum;

namespace Codemy.BL.ViewModel.Workshop
{
  public class GetCourseOutput : BaseVm
  {
    public string Title { get; set; }
    public string Description { get; set; }
    public string ImageSrc { get; set; }

    public Category_GetCourseOutput Category { get; set; }
    public ICollection<Tag_GetCourseOutput> Tags { get; set; }

  }

  public class Category_GetCourseOutput 
  {
    public string Title { get; set; }
    public string UrlTitle { get; set; }

  }

  public class Tag_GetCourseOutput
  {
    public string Title { get; set; }
    public string UrlTitle { get; set; }

  }
}
