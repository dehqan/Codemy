namespace Codemy.BL.ViewModel.Workshop
{
  public class GetCourseListItemOutput:BaseVm
  {
    public string Title { get; set; }
    public string Description { get; set; }
    public string ImageSrc { get; set; }
    public string Category { get; set; }
    public string UrlTitle { get; set; }


  }

}
