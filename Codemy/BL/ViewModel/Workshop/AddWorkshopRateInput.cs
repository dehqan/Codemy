namespace Codemy.BL.ViewModel.Workshop
{
    public class AddWorkshopRateInput
    {
        public string UserId { get; set; }
        public string WorkshopId { get; set; }
        public int Rate { get; set; }
    }
}