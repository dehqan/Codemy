namespace Codemy.BL.ViewModel.Workshop
{
    public class AddWorkshopCommentInput
    {
        public string UserId { get; set; }
        public string WorkshopId { get; set; }
        public string Text { get; set; }
    }
}