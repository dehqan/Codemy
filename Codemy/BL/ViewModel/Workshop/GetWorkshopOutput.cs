﻿using System;
using Codemy.Domain.Enum;

namespace Codemy.BL.ViewModel.Workshop
{
    public class GetWorkshopOutput : BaseVm
    {
        public string Title { get; set; }
        public int Duration { get; set; }
        public string ImageSrc { get; set; }
        public DateTime _holdingDateTime { private get; set; }
        public string HoldingDateTime => _holdingDateTime.ToShamsiDateTime();
        public int Cost { get; set; }
        public string Rate { get; set; }
        public WorkshopStatusEnum Status { get; set; }
        public string Course { get; set; }
        public long CourseId { get; set; }
        public string TeacherName { get; set; }
        public string TeacherId { get; set; }
        public string Description { get; set; }
        public string Goal { get; set; }
        public string PreRequirements { get; set; }
        public string TeacherDescription { get; set; }
    }
}
