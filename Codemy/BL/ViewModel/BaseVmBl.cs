using System;

namespace Codemy.BL.ViewModel
{
    public class BaseVm
    {
        public string Id { get; set; }
        public long Identity { get; set; }
        public DateTime CreateDateTime { get; set; }
        public string DateTime => CreateDateTime.ToShamsiDateTime();
        public string Date => DateTime.GetDateFromShamsiDateTime();
        public string Time => DateTime.GetTimeFromShamsiDateTime();
    }
}
