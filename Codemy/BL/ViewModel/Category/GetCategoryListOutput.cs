namespace Codemy.BL.ViewModel.Post
{
  public class GetCategoryListOutput : BaseVm
  {
    public string Title { get; set; }
    public string UrlTitle { get; set; }

  }
}
