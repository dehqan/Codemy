namespace Codemy.BL.ViewModel
{
  public class PaginationResult<T>
  {
    public T data { get; set; }
    public int currentPage { get; set; }
    public int totalPage { get; set; }
  }
}
