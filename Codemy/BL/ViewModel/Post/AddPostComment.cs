namespace Codemy.BL.ViewModel.Post
{
    public class AddPostComment
  {
        public string Text { get; set; }
        public string UserId { get; set; }
        public string PostId { get; set; }
    public string ParentId { get; set; }

  }
}
