using System.Collections.Generic;

namespace Codemy.BL.ViewModel.Post
{
  public class GetPostCommentListItemOutput : BaseVm
  {
    public string Text { get; set; }
    public decimal Rate { get; set; }
    public string Username { get; set; }
    public List<GetPostCommentListItemOutput> Children { get; set; }
  }
  
}
