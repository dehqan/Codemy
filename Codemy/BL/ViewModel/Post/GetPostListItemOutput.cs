namespace Codemy.BL.ViewModel.Post
{
  public class GetPostListItemOutput : BaseVm
  {
    public string Title { get; set; }
    public string Summary { get; set; }
    public string UrlTitle { get; set; }
    public string Category { get; set; }
    public string Image { get; set; }


  }

}
