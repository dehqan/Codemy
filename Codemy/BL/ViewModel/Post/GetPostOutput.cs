using System.Collections.Generic;
using Codemy.Domain.Enum;

namespace Codemy.BL.ViewModel.Post
{
  public class GetPostOutput : BaseVm
  {
    public string Title { get; set; }
    public string PostContent { get; set; }
    public string Summary { get; set; }
    public string UrlTitle { get; set; }
        public string Image { get; set; }
        public Category_GetPostOutput Category { get; set; }
    public PostTypeEnum PostType { get; set; }
    public ICollection<Tag_GetPostOutput> Tags { get; set; }

  }

  public class Category_GetPostOutput 
  {
    public string Title { get; set; }
    public string UrlTitle { get; set; }

  }

  public class Tag_GetPostOutput
  {
    public string Title { get; set; }
    public string UrlTitle { get; set; }

  }
}
