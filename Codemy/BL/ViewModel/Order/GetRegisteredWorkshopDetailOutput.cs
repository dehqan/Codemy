using System;

namespace Codemy.BL.ViewModel.Post
{
  public class GetRegisteredWorkshopDetailOutput : BaseVm
  {
    public string Title { get; set; }
    public bool IsPaid { get; set; }
    public int Price { get; set; }
    public string TransId { get; set; }
    public string Description { get; set; }
    public int Duration { get; set; }
    public decimal Rate { get; set; }
  }
  
}
