using System.ComponentModel.DataAnnotations.Schema;
using Codemy.Domain.Enum;

namespace Codemy.Domain.Entity
{
  public class BuyWorkshopOutput : BaseEntity
  {
    public OrderStatusEnum Status { get; set; }
    public string Description { get; set; }
    public string TransactionId { get; set; }
    public int PaymentRequestOutputStatus { get; set; }
    public string PaymentRequestOutputAuthority { get; set; }

    public string PaymentRequestAsyncDescription { get; set; }
    public string PaymentVerificationAsyncDescription { get; set; }

    public string UserId { get; set; }


    public string WorkshopIdentity { get; set; }

    }


}
