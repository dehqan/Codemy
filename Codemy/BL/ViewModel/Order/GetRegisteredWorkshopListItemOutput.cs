using System;
using Codemy.Domain.Enum;

namespace Codemy.BL.ViewModel.Post
{
    public class GetRegisteredWorkshopListItemOutput : BaseVm
    {
        public string Title { get; set; }
        public bool IsPaid => Status == OrderStatusEnum.Paid;
        public int Cost { get; set; }
        public OrderStatusEnum Status { get; set; }
        public DateTime HoldingDateTime { get; internal set; }
    }

}
