using System.Collections.Generic;

namespace Codemy.BL.ViewModel.Order
{
  public class BankConfiguration
  {
    public string Name { get; set; }
    public string MerchantID { get; set; }
    public List<BankDescription> BankDescriptions { get; set; }
    public string CallBackUrl { get; set; }
    public string BankUrl { get; set; }
  }

  public class BankDescription
  {
    public string Key { get; set; }
    public string Value { get; set; }
  }

  //public class PaymentRequestOutput
  //{
  //  public int Status { get; set; }
  //  public string Authority { get; set; }
  //}
}
