namespace Codemy.BL.ViewModel.Post
{
    public class BuyWorkshopInput
    {
        public long WorkshopIdentity { get; set; }
        public string UserId { get; set; }

    }
}
