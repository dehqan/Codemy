using AutoMapper;
using Codemy.BL.ViewModel.Post;
using Codemy.BL.ViewModel.Workshop;
using Codemy.Domain.Entity;

namespace Codemy.BL
{
  public class MappingProfile : Profile
  {
    public MappingProfile()
    {
      //CreateMap<ProductVm, Product>()
      //  .ForMember(dest => dest.ProductID, opt => opt.MapFrom(src => src.Id));
      //CreateMap<Product, ProductVm>()
      //  .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.ProductID));
      CreateMap<Post, GetPostListItemOutput>();
      CreateMap<PostComment, GetPostCommentListItemOutput>();
      CreateMap<Category, GetCategoryListOutput>();
      CreateMap<Course, GetCourseListItemOutput>();
      CreateMap<Workshop, GetWorkshopListOutput>();
      CreateMap<Workshop, GetWorkshopSummaryOutput>();
      CreateMap<Workshop, GetRelatedWorkshopListOutput>();
      CreateMap<Workshop, GetCourseWorkshopListOutput>();
      CreateMap<WorkshopComment, GetWorkshopCommentListOutput>();
      CreateMap<Order, GetRegisteredWorkshopListItemOutput>();

    }
  }
}
