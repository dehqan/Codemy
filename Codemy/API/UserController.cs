using System;
using Codemy.BL;
using Codemy.BL.Service.Interface;
using Codemy.DAL;
using Codemy.Domain.Enum;
using Codemy.Domain.IRepository;
using Microsoft.AspNetCore.Mvc;
using Codemy.API.Dto.User;
using Codemy.BL.ViewModel.User;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Codemy.BL.Exception;
using Microsoft.AspNetCore.Authorization;

namespace Codemy.API
{
    [Route("api/[controller]")]
    public class UserController : BaseController
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        ///پروفایل کاربر
        /// </summary>
        /// <remarks>علی</remarks>
        [HttpGet, Authorize, Route("GetProfile")]
        public async Task<IActionResult> GetProfile()
        {
           // throw new ProfileNotCompleted();
            try
            {
                var result = await _userService.GetProfileAsync(User.GetUserId());
                if (result == null)
                    return CustomError("کاربر یافت نشد", StatusCodeEnum.ApplicationError);

                return CustomResult(result);
            }
            catch (Exception exception)
            {
                return CustomError(exception);

            }
        }

        /// <summary>
        ///ویرایش پروفایل کاربر
        /// </summary>
        /// <remarks>علی</remarks>
        [HttpPost, Authorize, Route("EditProfile")]
        public async Task<IActionResult> EditProfile([FromBody] EditProfileDto model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return CustomError(ModelState);
                }

                var result = await _userService.EditProfileAsync(new EditProfileInput
                {
                    UserId = User.GetUserId(),
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Email = model.Email,
                    Avatar = model.Avatar
                });

                if (!result.isUpdated) CustomError("خطا در بروزرسانی", StatusCodeEnum.ApplicationError);
                if (!result.isEmailSent) CustomError("خطا در ارسال ایمیل", StatusCodeEnum.ApplicationError);

                return CustomResult();
            }
            catch (Exception exception)
            {
                return CustomError(exception);

            }
        }


        /// <summary>
        ///تماس با ما
        /// </summary>
        /// <remarks>مرتضی</remarks>
        [HttpPost, AllowAnonymous, Route("contact")]
        public IActionResult Contact([FromBody] ContactDto model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return CustomError(ModelState);
                }

                // _userService.AddContact(new ContactDto
                // {
                //     Email = model.Email,
                //     Text = model.Text,
                //     Title = model.Title
                // }, User.GetUserId());

                return CustomResult();
            }
            catch (Exception exception)
            {
                return CustomError(exception);

            }
        }


    }


}
