using System;
using System.Threading.Tasks;
using Codemy.BL.Pagination;
using Codemy.BL.Service.Interface;
using Microsoft.AspNetCore.Mvc;
using Codemy.BL;
using Codemy.BL.ViewModel.Post;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Codemy.Domain.Entity;
using Microsoft.Extensions.Configuration;

namespace Codemy.API
{
    [Route("api/[controller]")]
    public class OrderController : BaseController
    {
        private readonly IOrderService _orderService;
        private readonly IUserService _userService;

        public UserManager<AppUser> _userManager { get; }
        public IConfiguration _config { get; }

        public OrderController(IOrderService orderService,
            UserManager<AppUser> userManager,
            IUserService userService,
            IConfiguration config
        )
        {
            _orderService = orderService;
            _userManager = userManager;
            _userService = userService;
            _config = config;
        }

        /// <summary>
        ///خرید ورکشاپ
        /// </summary>
        /// <remarks>مرتضی</remarks>
        [HttpGet, Authorize, Route("buyworkshop/{id}")]
        public async Task<IActionResult> BuyWorkshop(long id)
        {
            try
            {


                var buyWorkshopResult = await _orderService.BuyWorkshop(new BuyWorkshopInput
                {
                    UserId = GetUserId(),
                    WorkshopIdentity = id
                });

                if (buyWorkshopResult.bankUrl == "") //Free Workshop
                {
                    var workshopIds = await _orderService.GetRegisteredWorkshopIdsAsync(GetUserId());
                    var user = await _userManager.FindByIdAsync(GetUserId());
                    var tokenString = BuildToken(_config, user, workshopIds);
                    return CustomResult(new
                    {
                        url = buyWorkshopResult.bankUrl,
                        order = buyWorkshopResult.order,
                        token = tokenString
                    });
                }
                return CustomResult(new
                {
                    url = buyWorkshopResult.bankUrl,
                    order = buyWorkshopResult.order
                });
            }
            catch (Exception exception)
            {
                return CustomError(exception);

            }
        }

        /// <summary>
        ///بازگشت از بانک
        /// </summary>
        /// <remarks>مرتضی</remarks>
        [HttpGet, Authorize, Route("bankCallback")]
        public async Task<IActionResult> BankCallback(string authority, string status)
        {
            try
            {
                var result = await _orderService.RedirectFromBank(authority, status);

                if (result.Status == Domain.Enum.OrderStatusEnum.Paid)
                {
                    var workshopIds = await _orderService.GetRegisteredWorkshopIdsAsync(GetUserId());
                    var user = await _userManager.FindByIdAsync(GetUserId());
                  //  var tokenString = BuildToken(_config, user, workshopIds);
                    _userService.sendBuyMessage(user.Id, result.WorkshopIdentity);
                    return CustomResult(new
                    {
                        order = result,
                    //    token = tokenString
                    });
                }

                return CustomResult(new
                {
                    order = result
                });

            }
            catch (Exception exception)
            {
                return CustomError(exception);

            }
        }

        /// <summary>
        ///لیست دوره های ثبت نامی یک فرد
        /// </summary>
        /// <remarks>مرتضی</remarks>
        [HttpGet, Authorize, Route("getRegisteredWorkshopList/{pageNumber}")]
        public IActionResult GetRegisteredWorkshopList(int pageNumber)
        {
            try
            {
                var page = new PageInput
                {
                    Page = pageNumber
                };

                var result = _orderService.GetRegisteredWorkshopList(page, User.GetUserId());

                return CustomResult(result);

            }
            catch (Exception exception)
            {
                return CustomError(exception);

            }
        }
        /// <summary>
        ///جزئیات دوره ثبت نام شده فرد
        /// </summary>
        /// <remarks>مرتضی</remarks>
        [HttpGet, Authorize, Route("getregisteredworkshopdetail/{orderId}")]
        public IActionResult GetRegisteredWorkshopDetail(string orderId)
        {
            try
            {
                var result = _orderService.GetRegisteredWorkshopDetail(orderId);
                return CustomResult(result);
            }
            catch (Exception exception)
            {
                return CustomError(exception);

            }
        }
    }
}
