using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Codemy.BL;
using Codemy.Domain.Entity;
using Codemy.Domain.Enum;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace Codemy.API
{
    public class BaseController : Controller
    {
        public BaseController()
        {
        }
        protected string GetUserId()
        {
            var res = User.Claims.First(x => x.Type == "UserId").Value;
            return res;
        }

        protected IActionResult CustomResult(object result = null)
        {
            return Ok(new CustomResultData
            {
                Status = StatusCodeEnum.Ok,
                Error = null,
                Result = result
            });
        }

        protected IActionResult CustomError(string error, StatusCodeEnum statusCode=StatusCodeEnum.UserError)
        {
            return CustomError(error, statusCode, null);
        }

        //protected IActionResult CustomError(object result, StatusCodeEnum statusCode)
        //{
        //    return CustomError(null, statusCode, result);
        //}

        protected IActionResult CustomError(Exception exception)
        {
            return CustomError(exception.Message, StatusCodeEnum.ApplicationError, exception.ToString());
        }

        protected IActionResult CustomError(ModelStateDictionary modelState)
        {
            var error = string.Join(", ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(modelError => modelError.ErrorMessage)
                    .ToList().ToArray());

            return CustomError(error, StatusCodeEnum.UserError, null);
        }

        protected IActionResult CustomError(IdentityResult identityResult)
        {
            var error = string.Join(", ", identityResult.Errors
                    .Select(x => x.Description)
                    .ToList().ToArray());

            return CustomError(error, StatusCodeEnum.ApplicationError, null);
        }

        private IActionResult CustomError(string errorMessage, StatusCodeEnum statusCode, object result)
        {
            return Ok(new CustomResultData
            {
                Status = statusCode,
                Error = errorMessage,
                Result = result
            });
        }

        protected string BuildToken(IConfiguration _config, AppUser user, List<string> worshopIds = null)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
                _config["Jwt:Issuer"],
                expires: DateTime.Now.AddHours(30),
                claims: new List<Claim> {
                    new Claim("UserId", user.Id.ToString()),
                    new Claim("Workshops", string.Join(',',worshopIds ?? new List<string>())),
                    new Claim("UserName", user.UserName),
                    new Claim("FullName",user.FirstName ?? "" + " " + user.LastName ?? ""),
                    new Claim("Avatar",user.Avatar ?? ""),
                    new Claim("Email",user.Email ?? "")

                },
                signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }


    }
}
