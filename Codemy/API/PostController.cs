using System;
using Codemy.API.Dto.Post;
using Codemy.BL.Pagination;
using Codemy.BL.Service.Interface;
using Codemy.BL.ViewModel.Post;
using Codemy.Domain.Enum;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Codemy.API
{
  [Route("api/[controller]")]
  public class PostController : BaseController
  {
    private readonly IPostService _postService;

    public PostController(IPostService postService)
    {
      _postService = postService;
    }

    /// <summary>
    /// متد لیست پست هایی که بلاگ هستند
    /// </summary>
    /// <remarks>مرتضی</remarks>

    [HttpGet, AllowAnonymous, Route("getbloglist/{pageNumber}")]
    public IActionResult GetBlogList(int pageNumber)
    {
      try
      {
        var page = new PageInput
        {
          Page = pageNumber
        };

        var result = _postService.GetPostList(page, PostTypeEnum.Blog, string.Empty);

        return CustomResult(result);

      }
      catch (Exception exception)
      {
        return CustomError(exception);

      }
    }

    /// <summary>
    /// متد لیست پست هایی که مرتبط با یک پست هستند
    /// </summary>
    /// <remarks>مرتضی</remarks>

    [HttpGet, AllowAnonymous, Route("getrelatedpostlist/{id}/{categoryId}/{pageNumber}")]
    public IActionResult GetRelatedPostList(string id, string categoryId, int pageNumber)
    {
      try
      {
        var page = new PageInput
        {
          Page = pageNumber
        };

        var result = _postService.GetRelatedPostList(page, id, categoryId);

        return CustomResult(result);
      }
      catch (Exception exception)
      {
        return CustomError(exception);

      }
    }

    /// <summary>
    /// متد لیست پست هایی که خبر هستند 
    /// </summary>
    /// <remarks>مرتضی</remarks>
    [HttpGet, AllowAnonymous, Route("getnewslist/{pageNumber}")]
    public IActionResult GetNewsList(int pageNumber)
    {
      try
      {
        var page = new PageInput
        {
          Page = pageNumber,
          OrderByVmProperty = "CreateDateTime",
          PageSize = 3
        };

        var result = _postService.GetPostList(page, PostTypeEnum.News, string.Empty);

        return CustomResult(result);

      }
      catch (Exception exception)
      {
        return CustomError(exception);

      }
    }

    /// <summary>
    /// نمایش جزئیات پست
    /// </summary>
    /// <remarks>مرتضی</remarks>
    [HttpGet, AllowAnonymous, Route("getpost/{id}/{urlTitle}")]
    public IActionResult GetPost(long id, string urlTitle)
    {
      try
      {

        var result = _postService.GetPost(id);

        return CustomResult(result);
      }
      catch (Exception exception)
      {
        return CustomError(exception);

      }
    }

    /// <summary>
    /// نمایش پست های صفحه اول
    /// </summary>
    /// <remarks>مرتضی</remarks>
    [HttpGet, AllowAnonymous, Route("gethomepostlist")]
    public IActionResult GetHomePostList()
    {
      try
      {

        var page = new PageInput
        {
          Page = 1,
          PageSize = 4
        };

        var result = _postService.GetPostList(page, PostTypeEnum.None, string.Empty, true);

        return CustomResult(result);

      }
      catch (Exception exception)
      {
        return CustomError(exception);

      }
    }


    /// <summary>
    /// نمایش کامنت های یک پست
    /// </summary>
    /// <remarks>مرتضی</remarks>
    [HttpGet, AllowAnonymous, Route("getPostCommentList/{postId}/{pageNumber}")]
    public IActionResult GetPostCommentList(string postId, int pageNumber)
    {
      try
      {

        var page = new PageInput
        {
          Page = pageNumber,
          OrderByVmProperty = "CreateDateTime",
        };

        var result = _postService.GetPostCommentList(postId, page);

        return CustomResult(result);

      }
      catch (Exception exception)
      {
        return CustomError(exception);

      }
    }

    /// <summary>
    ///اضافه کردن کامنت به پست
    /// </summary>
    /// <remarks>مرتضی</remarks>
    [HttpPost, Authorize, Route("AddPostComment")]
    public IActionResult AddPostComment([FromBody]AddPostCommentInputDto model)
    {
      try
      {
        if (!ModelState.IsValid)
          return CustomError(ModelState);

        GetPostCommentListItemOutput result = _postService.AddPostComment(new AddPostComment
        {
          Text = model.text,
           UserId = GetUserId(),

            PostId = model.id,
          ParentId = model.parentId

        });

        return CustomResult(result);
      }
      catch (Exception exception)
      {
        return CustomError(exception);
      }


    }

  }
}
