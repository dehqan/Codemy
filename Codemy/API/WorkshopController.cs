using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Codemy.BL.Pagination;
using Codemy.BL.Service.Interface;
using Microsoft.AspNetCore.Mvc;
using Codemy.Domain.Enum;
using Codemy.BL.ViewModel.Workshop;
using Codemy.API.Dto.Workshop;
using Codemy.BL;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;

namespace Codemy.API
{
    [Route("api/[controller]")]
    public class WorkshopController : BaseController
    {
        private readonly IWorkshopService _workshopService;
        private readonly IHostingEnvironment _env;

        public WorkshopController(IWorkshopService workshopService,
          IHostingEnvironment env
          )
        {
            _workshopService = workshopService;
            _env = env;
        }

        /// <summary>
        ///لیست کارگاه های صفحه اول
        /// </summary>
        /// <remarks>علی</remarks>
        [HttpGet, AllowAnonymous, Route("GetHomeWorkshopList")]
        public IActionResult GetHomeWorkshopList()
        {
            try
            {
                var page = new PageInput
                {
                    Page = 1
                };

                var result = _workshopService.GetWorkshopList(page, WorkshopStatusEnum.Queue);

                return CustomResult(result);
            }
            catch (Exception exception)
            {
                return CustomError(exception);

            }
        }

        /// <summary>
        ///لیست کارگاه ها
        /// </summary>
        /// <remarks>علی</remarks>
        [HttpGet, AllowAnonymous, Route("GetWorkshopList/{pageNumber}")]
        public IActionResult GetWorkshopList(int pageNumber)
        {
            try
            {
                var page = new PageInput
                {
                    Page = pageNumber,
                };

                var result = _workshopService.GetWorkshopList(page);

                return CustomResult(result);
            }
            catch (Exception exception)
            {
                return CustomError(exception);

            }
        }

        /// <summary>
        ///لیست کارگاه های مرتبط
        /// </summary>
        /// <remarks>علی</remarks>
        /// <param name="workshopId">شناسه کارگاه</param>
        /// <param name="categoryId">شناسه دسته بندی</param>
        /// <param name="pageNumber">شماره صفحه</param>
        [HttpGet, AllowAnonymous, Route("GetRelatedWorkshopList/{workshopId}/{categoryId}/{pageNumber}")]
        public IActionResult GetRelatedWorkshopList(string workshopId, string categoryId, int pageNumber)
        {
            try
            {
                var page = new PageInput
                {
                    Page = pageNumber
                };

                var result = _workshopService.GetRelatedWorkshopList(workshopId, categoryId, page);

                return CustomResult(result);
            }
            catch (Exception exception)
            {
                return CustomError(exception);

            }
        }

        /// <summary>
        ///خلاصه کارگاه
        /// </summary>
        /// <remarks>علی</remarks>
        /// <param name="workshopId">شناسه کارگاه</param>
        [HttpGet, AllowAnonymous, Route("GetWorkshopSummary/{workshopId}")]
        public IActionResult GetWorkshopSummary(string workshopId)
        {
            try
            {
                var result = _workshopService.GetWorkshopSummary(workshopId);
                if (result == null)
                    return CustomError("Invalid workshopId.", StatusCodeEnum.UserError);

                return CustomResult(result);
            }
            catch (Exception exception)
            {
                return CustomError(exception);

            }
        }

        /// <summary>
        ///کارگاه
        /// </summary>
        /// <remarks>علی</remarks>
        /// <param name="workshopId">شناسه کارگاه</param>
        [HttpGet, AllowAnonymous, Route("GetWorkshop/{workshopId}")]
        public IActionResult GetWorkshop(long workshopId)
        {
            try
            {
                var result = _workshopService.GetWorkshop(workshopId);
                if (result == null)
                    return CustomError("ورکشاپ وجود ندارد.", StatusCodeEnum.UserError);

                return CustomResult(result);
            }
            catch (Exception exception)
            {
                return CustomError(exception);

            }
        }

        /// <summary>
        ///کامنت های کارگاه
        /// </summary>
        /// <remarks>علی</remarks>
        /// <param name="workshopId">شناسه کارگاه</param>
        /// <param name="pageNumber">شماره صفحه</param>
        [HttpGet, AllowAnonymous, Route("GetWorkshopCommentList/{workshopId}/{pageNumber}")]
        public IActionResult GetWorkshopCommentList(string workshopId, int pageNumber)
        {
            try
            {
                var page = new PageInput
                {
                    Page = pageNumber
                };

                var result = _workshopService.GetWorkshopCommentList(workshopId, page);

                return CustomResult(result);
            }
            catch (Exception exception)
            {
                return CustomError(exception);

            }
        }

        /// <summary>
        ///لیست کارگاه های دوره
        /// </summary>
        /// <remarks>علی</remarks>
        /// <param name="courseId">شناسه دوره</param>
        /// <param name="pageNumber">شماره صفحه</param>
        [HttpGet, AllowAnonymous, Route("GetCourseWorkshopList/{courseId}/{pageNumber}")]
        public IActionResult GetCourseWorkshopList(string courseId, int pageNumber)
        {
            try
            {
                var page = new PageInput
                {
                    Page = pageNumber
                };

                var result = _workshopService.GetCourseWorkshopList(courseId, page);

                return CustomResult(result);
            }
            catch (Exception exception)
            {
                return CustomError(exception);

            }
        }

        /// <summary>
        ///اضافه کردن کامنت کارگاه
        /// </summary>
        /// <remarks>علی</remarks>
        [HttpPost, Authorize, Route("AddWorkShopComment")]
        public IActionResult AddWorkShopComment([FromBody]AddWorkShopCommentDto dto)
        {
            try
            {
                if (!ModelState.IsValid)
                    return CustomError(ModelState);

                var input = new AddWorkshopCommentInput
                {
                    UserId = User.GetUserId(),
                    WorkshopId = dto.Id,
                    Text = dto.Text,
                };
                var result = _workshopService.AddWorkshopComment(input);

                return CustomResult(result);
            }
            catch (Exception exception)
            {
                return CustomError(exception);

            }
        }

        /// <summary>
        ///اضافه کردن امتیاز کارگاه
        /// </summary>
        /// <remarks>علی</remarks>
        [HttpPost, Authorize, Route("AddWorkShopRate")]
        public IActionResult AddWorkShopRate([FromBody]AddWorkShopRateDto dto)
        {
            try
            {
                if (!ModelState.IsValid)
                    return CustomError(ModelState);

                var input = new AddWorkshopRateInput
                {
                    UserId = User.GetUserId(),
                    WorkshopId = dto.WorkshopId,
                    Rate = dto.Rate,
                };
                _workshopService.AddWorkshopRate(input);

                return CustomResult();
            }
            catch (Exception exception)
            {
                return CustomError(exception);

            }
        }

        /// <summary>
        ///دانلود فایل ضمیمه ورکشاپ
        /// </summary>
        /// <remarks>مرتضی</remarks>
        [HttpGet, Authorize, Route("getdownloadlink/{workshopFileId}")]
        public async Task<IActionResult> GetDownloadLink(string workshopFileId)
        {
            var webRoot = _env.WebRootPath;

            var relativeFilePath = await _workshopService.DownloadworkshopFileId(User.GetUserId(), workshopFileId);
            var file = System.IO.Path.Combine(webRoot, relativeFilePath);
            var ext = Path.GetExtension(file);
            var fileBytes = System.IO.File.ReadAllBytes(file);
            var fileName = string.Format("file-{0:yyyy-MM-dd_hh-mm-ss-tt}" + ext, DateTime.Now);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        /// <summary>
        ///نمایش لیست دوره ها
        /// </summary>
        /// <remarks>مرتضی</remarks>
        [HttpGet, AllowAnonymous, Route("getcourselist/{pageNumber}")]
        public IActionResult GetCourseList(int pageNumber)
        {
            try
            {
                var page = new PageInput
                {
                    Page = pageNumber
                };

                var result = _workshopService.GetCourseList(page);

                return CustomResult(result);
            }
            catch (Exception exception)
            {
                return CustomError(exception);

            }
        }

        /// <summary>
        ///نمایش جزئیات یک دوره
        /// </summary>
        /// <remarks>مرتضی</remarks>
        [HttpGet, AllowAnonymous, Route("getcourse/{id}/{urlTitle}")]
        public IActionResult GetCourse(long id, string urlTitle)
        {
            try
            {

                var result = _workshopService.GetCourse(id);

                return CustomResult(result);
            }
            catch (Exception exception)
            {
                return CustomError(exception);

            }
        }


        /// <summary>
        ///چک کردن ظرفیت و تاریخ دوره
        /// </summary>
        /// <remarks>مرتضی</remarks>
        [HttpGet, AllowAnonymous, Route("isWorkshopAvailable/{id}")]
        public async Task<IActionResult> IsWorkshopAvailable(long id)
        {
            try
            {

                var hasCapacity = await _workshopService.hasCapacity(id);
                bool isExpired = await _workshopService.isExpired(id);

                if (hasCapacity) return CustomResult(new { text = "ظرفیت تکمیل شده است؛ تمایل به خرید آفلاین دوره دارید؟", result = false });
                if (isExpired) return CustomResult(new { text = "زمان ثبت نام به پایان رسیده است؛ تمایل به خرید آفلاین دوره دارید؟", result = false });

                return CustomResult(new {result = true });

            }
            catch (Exception exception)
            {
                return CustomError(exception);

            }
        }
    }
}
