using System.ComponentModel.DataAnnotations;

namespace Codemy.API.Dto.Post
{
  public class AddPostCommentInputDto
  {
    [Required]
    public string text { get; set; }

    [Required]
    public string id { get; set; }

    public string parentId { get; set; }
  }
}
