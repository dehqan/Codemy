
namespace Codemy.API.Dto.Workshop
{
    public class AddWorkShopRateDto
    {
        public string WorkshopId { get; set; }
        public int Rate { get; set; }
    }
}