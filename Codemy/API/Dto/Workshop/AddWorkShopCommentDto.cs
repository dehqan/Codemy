using System.ComponentModel.DataAnnotations;

namespace Codemy.API.Dto.Workshop
{
    public class AddWorkShopCommentDto
    {
        [Required]
        public string Id { get; set; }

        [Required]
        public string Text { get; set; }
    }
}