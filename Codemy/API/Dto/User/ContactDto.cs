using System.ComponentModel.DataAnnotations;

namespace Codemy.API.Dto.User
{
    public class ContactDto
  {
        public string Title { get; set; }
        public string Text { get; set; }

        [EmailAddress]
        public string Email { get; set; }

    }
}
