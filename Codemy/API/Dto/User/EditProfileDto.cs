using System.ComponentModel.DataAnnotations;

namespace Codemy.API.Dto.User
{
    public class EditProfileDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        public string Avatar { get; set; }
    }
}