using System.ComponentModel.DataAnnotations;

namespace Codemy.API.Dto.Security
{
    public class LoginDto
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }
}