﻿using System.ComponentModel.DataAnnotations;

namespace Codemy.API.Dto.Security
{
    public class RegisterDto
    {
        [RegularExpression("(\\+98|0)?9\\d{9}",ErrorMessage ="شماره موبایل فرمت صحیح ندارد.")]
        public string Username { get; set; }
    }
}