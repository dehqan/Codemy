using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Codemy.API.Dto.Security;
using Codemy.BL.Service.Interface;
using Codemy.Domain.Entity;
using Codemy.Domain.Enum;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace Codemy.API
{
    [Route("api/[controller]")]
    public class SecurityController : BaseController
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signinManager;
        private readonly ISmsService _smsService;
        private readonly IConfiguration _config;

        public IOrderService _orderService { get; }

        public SecurityController(IConfiguration config,
            UserManager<AppUser> userManager,
            SignInManager<AppUser> signinManager,
            ISmsService smsService,
            IOrderService orderService
            )
        {
            _config = config;
            _userManager = userManager;
            _signinManager = signinManager;
            _smsService = smsService;
            _orderService = orderService;
        }

        /// <summary>
        ///ثبت نام
        /// </summary>
        /// <remarks>علی</remarks>
        [HttpPost, AllowAnonymous, Route("Register")]
        public async Task<IActionResult> Register([FromBody]RegisterDto model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return CustomError(ModelState);

                var random = new Random();
                var verificationCode = random.Next(100000, 999999).ToString();

                var user = await _userManager.FindByNameAsync(model.Username);
                if (user == null) //register
                {
                    user = new AppUser
                    {
                        UserName = model.Username,
                        Mobile = model.Username,
                        LastRegisterationDateTime = DateTime.Now
                    };

                    var createResult = await _userManager.CreateAsync(user, verificationCode);
                    if (!createResult.Succeeded)
                        return CustomError("مشکل در ثبت کاربر",StatusCodeEnum.ApplicationError);
                }
                else
                {
                    //check for 1min between sending sms
                    if (user.LastRegisterationDateTime != null)
                    {
                        var dif = DateTime.Now - user.LastRegisterationDateTime.Value;
                        if (dif.TotalMinutes < 1)
                        {
                            if (dif.TotalSeconds != 0)
                            {
                                var span = new TimeSpan(0, 0, (int)(Math.Abs(dif.TotalSeconds - 60)));
                                var remainigSeconds = Convert.ToInt32(span.ToString(@"ss"));
                                return CustomError($"زمان باقیمانده تا ارسال مجدد {remainigSeconds.ToString()}", StatusCodeEnum.UserError);
                            }
                        }
                    }

                    //update password
                    user.PasswordHash = _userManager.PasswordHasher.HashPassword(user, verificationCode);
                    user.LastRegisterationDateTime = DateTime.Now;

                    var updateResult = await _userManager.UpdateAsync(user);
                    if (!updateResult.Succeeded)
                        return CustomError(updateResult);
                }

                //send sms
                _smsService.SendVerificationCode(user.Id, user.UserName, verificationCode);

                return CustomResult();
            }
            catch (Exception exception)
            {
                return CustomError(exception);
            }
        }

        /// <summary>
        ///ورود
        /// </summary>
        /// <remarks>علی</remarks>
        [HttpPost, AllowAnonymous, Route("Login")]
        public async Task<IActionResult> Login([FromBody]LoginDto model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return CustomError(ModelState);

                var user = await _userManager.FindByNameAsync(model.Username);
                if (user == null) return CustomError("لطفا شماره تماس خود را وارد کنید", StatusCodeEnum.UserError);

                if (!await _userManager.CheckPasswordAsync(user, model.Password))
                    return CustomError("نام کاربری یا رمز عبور اشتباه است!", StatusCodeEnum.UserError);

                var workshopIds=await _orderService.GetRegisteredWorkshopIdsAsync(user.Id);
                var tokenString = BuildToken(_config,user,workshopIds);

                return CustomResult(tokenString);
            }
            catch (Exception exception)
            {
                return CustomError(exception);
            }
        }

        /// <summary>
        ///خروج
        /// </summary>
        /// <author>Ali</author>
        [HttpPost, Authorize, Route("Logout")]
        public async Task<IActionResult> Logout()
        {
            try
            {
                await _signinManager.SignOutAsync();
                return CustomResult();
            }
            catch (Exception exception)
            {
                return CustomError(exception);
            }
        }

        /// <summary>
        ///تایید ایمیل
        /// </summary>
        /// <remarks>علی</remarks>
        [HttpGet, Authorize, Route("ConfirmEmail")]
        public async Task<IActionResult> ConfirmEmail(string userId, string token)
        {
            if (userId == null || token == null)
                return CustomError("خطا", StatusCodeEnum.UserError);

            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
                return CustomError("خطا", StatusCodeEnum.UserError);

            var result = await _userManager.ConfirmEmailAsync(user, token);

            return CustomResult(new { confirmed = result.Succeeded });
        }

     
    }
}
