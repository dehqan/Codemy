using System;
using Codemy.BL.Pagination;
using Codemy.BL.Service.Interface;
using Codemy.Domain.Enum;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Codemy.API
{
  [Route("api/[controller]")]
  public class CategoryController : BaseController
  {
    private readonly ICategoryService _categoryService;


    public CategoryController(ICategoryService categoryService)
    {
      _categoryService = categoryService;
    }
    /// <summary>
    ///لیست کتگوری های مربوط به پست ها
    /// </summary>
    /// <remarks>مرتضی</remarks>
    [HttpGet, AllowAnonymous, Route("getpostcategorylist/{pageNumber}")]
    public IActionResult GetPostCategoryList(int pageNumber)
    {
      try
      {
        var page = new PageInput
        {
          Page = pageNumber
        };

        var result = _categoryService.GetCategoryList(page,CategoryTypeEnum.Post);

        return CustomResult(result);

      }
      catch (Exception exception)
      {
        return CustomError(exception);

      }
    }

    /// <summary>
    ///لیست کتگوری های مربوط به دوره ها
    /// </summary>
    /// <remarks>مرتضی</remarks>
    [HttpGet, AllowAnonymous, Route("getcoursecategorylist/{pageNumber}")]
    public IActionResult GetCourseCategoryList(int pageNumber)
    {
      try
      {
        var page = new PageInput
        {
          Page = pageNumber
        };

        var result = _categoryService.GetCategoryList(page, CategoryTypeEnum.Course);

        return CustomResult(result);

      }
      catch (Exception exception)
      {
        return CustomError(exception);

      }
    }
  }
}
