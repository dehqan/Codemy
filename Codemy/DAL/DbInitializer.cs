using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Codemy.Domain.Entity;
using Codemy.Domain.Enum;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Codemy.DAL
{
  public class DbInitializer 
  {
    //https://www.locktar.nl/programming/net-core/seed-database-users-roles-dotnet-core-2-0-ef/
    public static async Task Initialize(CodemyDbContext Context, UserManager<AppUser> UserManager,
      RoleManager<AppRole> RoleManager)
    {

      Context.Database.EnsureCreated();

      #region Add roles

      var roles = new List<AppRole>();
      var dbRoles = Context.Roles.ToList();
      if (dbRoles.All(x => x.Name != "admin"))
        await RoleManager.CreateAsync(new AppRole()
        {
          Name = "admin",

        });

      if (dbRoles.All(x => x.Name != "teacher"))
        await RoleManager.CreateAsync(new AppRole()
        {
          Name = "teacher",

        });
      if (dbRoles.All(x => x.Name != "student"))
        await RoleManager.CreateAsync(new AppRole()
        {
          Name = "student",

        });
      if (dbRoles.All(x => x.Name != "user"))
        await RoleManager.CreateAsync(new AppRole()
        {
          Name = "user",

        });


      #endregion

      #region Add users

      var dbUsers = Context.Users.ToList();

      if (dbUsers.All(x => x.UserName != "admin"))
        await UserManager.CreateAsync(new AppUser()
        {
          EmailConfirmed = true,
          PhoneNumberConfirmed = true,
          IsProfileCompleted = true,
          FirstName = "admin",
          LastName = "admin",
          PhoneNumber = "09122043657",
          UserName = "admin",
          Email = "mortezadalil@gmail.com",
        });


      if (dbUsers.All(x => x.UserName != "babak"))
        await UserManager.CreateAsync(new AppUser()
        {
          EmailConfirmed = true,
          PhoneNumberConfirmed = true,
          IsProfileCompleted = true,
          FirstName = "بابک",
          LastName = "دلیل",
          PhoneNumber = "09122198126",
          UserName = "babak",
          Email = "babak@gmail.com",
        });


      if (dbUsers.All(x => x.UserName != "mortezadalil"))
        await UserManager.CreateAsync(new AppUser()
        {
          EmailConfirmed = true,
          PhoneNumberConfirmed = true,
          IsProfileCompleted = true,
          FirstName = "مرتضی",
          LastName = "دلیل",
          PhoneNumber = "09022043657",
          UserName = "mortezadalil",
          Email = "mortezadalil@yahoo.com"
        });



      #endregion

      #region Add userRole

      var admin = Context.Users.FirstOrDefault(x => x.UserName == "admin");
      var isadmininAdminRole =
       await UserManager.IsInRoleAsync(admin, "admin");
      if (!isadmininAdminRole)
      {
        await UserManager.AddToRoleAsync(admin, "admin");
      }

      var teacher1 = Context.Users.FirstOrDefault(x => x.UserName == "mortezadalil");
      var ismortezadalilInnTeacherRole =
        await UserManager.IsInRoleAsync(admin, "teacher");
      if (!ismortezadalilInnTeacherRole)
      {
        await UserManager.AddToRolesAsync(teacher1, new List<string>() { "user", "teacher", "student" });
      }

      var student = Context.Users.FirstOrDefault(x => x.UserName == "babak");
      var isnanakInnStudentRole =
        await UserManager.IsInRoleAsync(admin, "student");
      if (!isnanakInnStudentRole)
      {
        await UserManager.AddToRolesAsync(student, new List<string>() { "user", "student" });
      }
      #endregion

      #region Add Default Categories

      var categories = new List<Category>();
      var dbCategories = Context.Categories.ToList();

      if (dbCategories.All(x => x.Title != "روزانه"))
        categories.Add(new Category()
        {
          UrlTitle = "روزانه",
          Title = "روزانه",
          Type = CategoryTypeEnum.Post
        });
      if (dbCategories.All(x => x.Title != "آموزشی"))
        categories.Add(new Category()
        {
          UrlTitle = "آموزشی",
          Title = "آموزشی",
          Type = CategoryTypeEnum.Post
        });
      if (dbCategories.All(x => x.Title != "دوره آنلاین"))

        categories.Add(new Category()
        {
          UrlTitle = "دوره-آنلاین",
          Title = "دوره آنلاین",
          Type = CategoryTypeEnum.Post,

        });
      if (dbCategories.All(x => x.Title != ".Net"))
        categories.Add(new Category()
        {
          UrlTitle = "dotnet",
          Title = ".Net",
          Type = CategoryTypeEnum.Course
        });
      if (dbCategories.All(x => x.Title != "Angular"))
        categories.Add(new Category()
        {
          UrlTitle = "angular",
          Title = "Angular",
          Type = CategoryTypeEnum.Course
        });


      Context.Categories.AddRange(categories);
      Context.SaveChanges();



      #endregion

      #region Add Default Tags

      if (!Context.Tags.Any())
      {
        var tags = new List<Tag>()
        {
          new Tag() {Title = "c#", UrlTitle = "csharp"},
          new Tag() {Title = ".Net", UrlTitle = "dotnet"},
          new Tag() {Title = ".Net Core", UrlTitle = "dotnetcore"},
          new Tag() {Id = Guid.NewGuid().ToString(), Title = "Angular", UrlTitle = "angular"},
          new Tag() {Title = "علمی", UrlTitle = "علمی"},
          new Tag() {Title = "دوره", UrlTitle = "دوره"},
          new Tag() {Title = "آموزشی", UrlTitle = "آموزشی"},
          new Tag() {Title = "ویدیو", UrlTitle = "ویدیو"},
          new Tag() {Title = "معرفی کتاب", UrlTitle = "معرفی-کتاب"},
        };
        Context.Tags.AddRange(tags);
        Context.SaveChanges();
      }

      #endregion

      #region Add Course

      var courses = Context.Courses.ToList();

      if(courses.All(x => x.UrlTitle != "angular"))
      Context.Courses.Add(new Course()
      {
        Title = "Angular",
        Description = "انگولار یک فریم ورک فرانت اندی است.",
        CategoryId = dbCategories.FirstOrDefault(x => x.UrlTitle == "angular")?.Id,
        UrlTitle = "angular"
      });
      if (courses.All(x => x.UrlTitle != "asp-core"))
        Context.Courses.Add(new Course()
      {
        Title = "Asp Core",
        Description = "دات نت یک فریم ورک بک اندی است.",
        CategoryId = dbCategories.FirstOrDefault(x => x.UrlTitle == "dotnet")?.Id,
        UrlTitle = "asp-core"
       
      });

      Context.SaveChanges();
      #endregion

      #region Add CourseTag

      var courseTags = Context.CourseTags.ToList();
      var courseId = Context.Courses.FirstOrDefault(x => x.UrlTitle == "angular")?.Id;
      var tagId1 = Context.Tags.FirstOrDefault(x => x.UrlTitle == "angular")?.Id;
      var tagId2 = Context.Tags.FirstOrDefault(x => x.UrlTitle == "آموزشی")?.Id;

      if(!courseTags.Any(x=>x.TagId==tagId1 && x.CourseId==courseId))
      Context.CourseTags.Add(new CourseTag()
      {
        CourseId = courseId,
        TagId = tagId1
      });
      if (!courseTags.Any(x => x.TagId == tagId2 && x.CourseId == courseId))
        Context.CourseTags.Add(new CourseTag()
      {
        CourseId = courseId,
        TagId = tagId1
      });

      Context.SaveChanges();

      #endregion

      #region Add Workshop

      var workshops = Context.Workshops.ToList();

      if (workshops.All(x => x.Title != "کارگاه آموزش Redux در انگولار"))
        Context.Workshops.Add(new Workshop()
      {
        Cost = 0,
        Status = WorkshopStatusEnum.Queue,
        Description = "دوره آموزشی",
        Title = "کارگاه آموزش Redux در انگولار",
        CourseId = Context.Courses.FirstOrDefault(x => x.UrlTitle == "angular")?.Id,
        Goal = "هدف از این دوره",
        Duration = 90,
        PreRequirements = "پیش نیاز این دوره",
        TeacherId = Context.Users.FirstOrDefault(x => x.UserName == "mortezadalil")?.Id,
      });
      if (workshops.All(x => x.Title != "معرفی دات نت کور 2"))

        Context.Workshops.Add(new Workshop()
      {
        Cost = 0,
        Status = WorkshopStatusEnum.Queue,
        Description = "دوره آموزشی",
        Title = "معرفی دات نت کور 2",
        CourseId = Context.Courses.FirstOrDefault(x => x.UrlTitle == "asp-core")?.Id,
        Goal = "هدف از این دوره",
        Duration = 90,
        PreRequirements = "پیش نیاز این دوره",
        TeacherId = Context.Users.FirstOrDefault(x => x.UserName == "mortezadalil")?.Id,
      });
      Context.SaveChanges();

      #endregion

      #region Add WorkshopTag

      var workshopTags = Context.WorkShopTags.ToList();
      var workshopId = Context.Workshops.FirstOrDefault(x => x.Title == "کارگاه آموزش Redux در انگولار")?.Id;
      var tagId3 = Context.Tags.FirstOrDefault(x => x.UrlTitle == "angular")?.Id;
      var tagId4 = Context.Tags.FirstOrDefault(x => x.UrlTitle == "آموزشی")?.Id;

      if (!workshopTags.Any(x => x.TagId == tagId3 && x.WorkshopId == workshopId))
       Context.WorkShopTags.Add(new WorkshopTag()
        {
          WorkshopId = workshopId,
          TagId = tagId3
        });
      if (!workshopTags.Any(x => x.TagId == tagId4 && x.WorkshopId == workshopId))
        Context.WorkShopTags.Add(new WorkshopTag()
        {
          WorkshopId = workshopId,
          TagId = tagId4
        });

      

      Context.SaveChanges();

      #endregion
      #region Add Default Posts (With one savechange)

      if (!Context.Posts.Any())
      {
        var post = new Post()
        {
          Title = "عنوان بلاگ در مورد انگولار",
          UrlTitle = "عنوان-بلاگ-در-مورد-انگولار",
          PostContent = "محتوای بلاگ",
          Summary = "خلاصه بلاگ",
          PostType = PostTypeEnum.Blog,
        };
        var tag = Context.Tags.FirstOrDefault(x => x.UrlTitle == "angular");
        var category = Context.Categories.FirstOrDefault(x => x.UrlTitle == "angular");
        var postTag1 = new PostTag()
        {
          Post = post,
          Tag = tag
        };

        post.PostTags = new List<PostTag>() { postTag1 };
        post.Category = category;

        Context.Posts.Add(post);
        Context.SaveChanges();
      }
      #endregion



    }
  }
}

