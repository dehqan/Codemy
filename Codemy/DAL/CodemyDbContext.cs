using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Codemy.Domain.Entity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Codemy.DAL
{
    public class CodemyDbContext : IdentityDbContext<AppUser, AppRole, string>
    {

        public CodemyDbContext(DbContextOptions<CodemyDbContext> options) : base(options)
        {

        }

        public DbSet<AppUser> UserDetails { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<CourseTag> CourseTags { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<PostTag> PostTags { get; set; }
        public DbSet<PostComment> PostComments { get; set; }
        public DbSet<Session> Sessions { get; set; }
        public DbSet<Workshop> Workshops { get; set; }
        public DbSet<WorkshopComment> WorkshopComments { get; set; }
        public DbSet<WorkshopRate> WorkshopRates { get; set; }
        public DbSet<WorkshopTag> WorkShopTags { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<DownloadLink> DownloadLinks { get; set; }
        public DbSet<WorkshopFile> WorkshopFiles { get; set; }
        public DbSet<SmsLog> SmsLogs { get; set; }


      public DbSet<Contact> Contacts { get; set; }


    protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Turning off casecade delete
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

            // in every update of 'user' table, 'identity' column updates automatically which it shouldn't
            // this line ignores updating of this column
            modelBuilder.Entity<AppUser>().Property(p => p.Identity)
                .Metadata.AfterSaveBehavior = PropertySaveBehavior.Ignore;

            // Global Query for IsDeleted
            foreach (var entityType in modelBuilder.Model.GetEntityTypes())
            {
                var find = entityType.FindProperty("IsDeleted");
                if (find == null) continue;

                // 1. Add the IsDeleted property
                entityType.GetOrAddProperty("IsDeleted", typeof(bool));

                // 2. Create the query filter

                var parameter = Expression.Parameter(entityType.ClrType);

                // EF.Property<bool>(post, "IsDeleted")
                var propertyMethodInfo = typeof(EF).GetMethod("Property").MakeGenericMethod(typeof(bool));
                var isDeletedProperty = Expression.Call(propertyMethodInfo, parameter, Expression.Constant("IsDeleted"));

                // EF.Property<bool>(post, "IsDeleted") == false
                BinaryExpression compareExpression = Expression.MakeBinary(ExpressionType.Equal, isDeletedProperty, Expression.Constant(false));

                // post => EF.Property<bool>(post, "IsDeleted") == false
                var lambda = Expression.Lambda(compareExpression, parameter);

                modelBuilder.Entity(entityType.ClrType).HasQueryFilter(lambda);
            }

            base.OnModelCreating(modelBuilder);
        }
    }
}
