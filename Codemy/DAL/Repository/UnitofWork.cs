using System;
using Codemy.Domain.IRepository;

namespace Codemy.DAL.Repository
{
  public sealed class UnitOfWork : IUnitOfWork
  {
    private CodemyDbContext _context;

    public UnitOfWork(CodemyDbContext context)
    {
      _context = context;
    }

    public IPostRepository PostRepository => new PostRepository(_context);
    public IOrderRepository OrderRepository => new OrderRepository(_context);
    public ICategoryRepository CategoryRepository => new CategoryRepository(_context);
    public IWorkshopRepository WorkshopRepository => new WorkshopRepository(_context);
    public IWorkshopFileRepository WorkshopFileRepository => new WorkshopFileRepository(_context);
    public IDownloadLinkRepository DownloadLinkRepository => new DownloadLinkRepository(_context);

    public IWorkshopCommentRepository WorkshopCommentRepository => new WorkshopCommentRepository(_context);
    public IWorkshopRateRepository WorkshopRateRepository => new WorkshopRateRepository(_context);
    public IPostCommentRepository PostCommentRepository => new PostCommentRepository(_context);
    public ISmsLogRepository SmsLogRepository => new SmsLogRepository(_context);
    public IContactRepository ContactRepository => new ContactRepository(_context);
    public ICourseRepository CourseRepository => new CourseRepository(_context);


    public void Save()
    {
      _context.SaveChanges();
    }

    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    public void Dispose(bool disposing)
    {
      if (disposing)
      {
        if (_context != null)
        {
          _context.Dispose();
          _context = null;
        }
      }
    }
  }

}

