using Codemy.Domain.Entity;
using Codemy.Domain.IRepository;

namespace Codemy.DAL.Repository
{
  public class PostRepository : Repository<Post>, IPostRepository
  {
    private readonly CodemyDbContext _context;

    public PostRepository(CodemyDbContext context) : base(context)
    {
      _context = context;
    }
  }

  public class OrderRepository : Repository<Order>, IOrderRepository
  {
    private readonly CodemyDbContext _context;

    public OrderRepository(CodemyDbContext context) : base(context)
    {
      _context = context;
    }
  }
  public class CategoryRepository : Repository<Category>, ICategoryRepository
  {
    private readonly CodemyDbContext _context;

    public CategoryRepository(CodemyDbContext context) : base(context)
    {
      _context = context;
    }
  }

  public class WorkshopRepository : Repository<Workshop>, IWorkshopRepository
  {
    private readonly CodemyDbContext _context;

    public WorkshopRepository(CodemyDbContext context) : base(context)
    {
      _context = context;
    }
  }
  public class WorkshopFileRepository : Repository<WorkshopFile>, IWorkshopFileRepository
  {
    private readonly CodemyDbContext _context;

    public WorkshopFileRepository(CodemyDbContext context) : base(context)
    {
      _context = context;
    }
  }
  public class DownloadLinkRepository : Repository<DownloadLink>, IDownloadLinkRepository
  {
    private readonly CodemyDbContext _context;

    public DownloadLinkRepository(CodemyDbContext context) : base(context)
    {
      _context = context;
    }
  }
  
  public class WorkshopCommentRepository : Repository<WorkshopComment>, IWorkshopCommentRepository
  {
    private readonly CodemyDbContext _context;

    public WorkshopCommentRepository(CodemyDbContext context) : base(context)
    {
      _context = context;
    }
  }

  public class WorkshopRateRepository : Repository<WorkshopRate>, IWorkshopRateRepository
  {
    private readonly CodemyDbContext _context;

    public WorkshopRateRepository(CodemyDbContext context) : base(context)
    {
      _context = context;
    }
  }

  public class PostCommentRepository : Repository<PostComment>, IPostCommentRepository
  {
    private readonly CodemyDbContext _context;

    public PostCommentRepository(CodemyDbContext context) : base(context)
    {
      _context = context;
    }
  }

  public class SmsLogRepository : Repository<SmsLog>, ISmsLogRepository
  {
    private readonly CodemyDbContext _context;

    public SmsLogRepository(CodemyDbContext context) : base(context)
    {
      _context = context;
    }
  }
  public class ContactRepository : Repository<Contact>, IContactRepository
  {
    private readonly CodemyDbContext _context;

    public ContactRepository(CodemyDbContext context) : base(context)
    {
      _context = context;
    }
  }

  public class CourseRepository : Repository<Course>, ICourseRepository
  {
    private readonly CodemyDbContext _context;

    public CourseRepository(CodemyDbContext context) : base(context)
    {
      _context = context;
    }
  }

}
