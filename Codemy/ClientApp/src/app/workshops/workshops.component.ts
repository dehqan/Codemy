import { Component, OnInit } from '@angular/core';
import { BaseService } from '../core/service/base.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { BasePage } from '../core/basePage';
import { GetWorkshopListOutput } from './models/getWorkshopListOutput';


@Component({
  selector: 'app-workshops',
  templateUrl: './workshops.component.html',
})
export class WorkshopsComponent extends BasePage implements OnInit {

  constructor(protected baseService: BaseService,
    protected toastr: ToastrService,
    private router: Router
  ) {
    super(baseService, toastr);
  }

  page: number = 1;
  serverTotalpages: number;
  serverCurrentPageNumber: number;
  workshops: Array<GetWorkshopListOutput> = [];

  ngOnInit() {
    super.ngOnInit();
    this.getWorkshops();
  }

  nextPage() {
    this.page++;
    this.getWorkshops();
  }

  getWorkshops() {
    this.getListWithPagination<GetWorkshopListOutput>("workshop/getWorkshopList/" + this.page)
      .subscribe(x => {
        this.serverCurrentPageNumber = x.pageNumber;
        this.workshops = this.workshops.concat(x.results);

        if (this.page == 1 && this.workshops.length > 0) {
          this.serverTotalpages = x.totalNumberOfPages;
        }
      });

  }
 

}
