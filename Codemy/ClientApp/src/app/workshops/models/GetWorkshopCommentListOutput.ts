import { BaseVm } from "../../core/model/baseModel";

export class GetWorkshopCommentListOutput extends BaseVm {
  text: string;
  userDetail: string;
  userId: string;
  workshop: string;
  workshopId: number;
  children: GetWorkshopCommentListOutput[];

  firstReply?: GetWorkshopCommentListOutput;
  inPage: number;
}
