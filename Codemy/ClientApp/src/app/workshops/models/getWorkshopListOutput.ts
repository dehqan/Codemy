import { BaseVm } from "../../core/model/baseModel";

export class GetWorkshopListOutput extends BaseVm {
    title: string;
    urlTitle: string;
    duration: number;
    imageSrc: string;
    holdingDateTime: string;
    cost: number;
    rate: number;
    status: number;
    course: string;
    courseId: number;
    userDetail: string;
    teacherId: number;
}