import { BaseVm } from "../../core/model/baseModel";

export class GetWorkshopOutput extends BaseVm {
    title: string;
    description: string;
    goal: string;
    preRequirements: string;
    duration: number;
    imageSrc: string;
    holdingDateTime: string;
    cost: number;
    rate: string;
    status: number;
    course: string;
    courseId: number;
    userDetail: string;
    teacherId: number;
    teacherDescription: string;

    isRegistered?:boolean;
    downloadPercent?:number;
    downloadUrl?:string;
    downloadFileName?:string;
}
