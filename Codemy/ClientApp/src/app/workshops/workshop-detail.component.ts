import { AuthService } from './../shared/auth.service';
import { Component, OnInit } from '@angular/core';
import { BasePage } from '../core/basePage';
import { BaseService } from '../core/service/base.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GetWorkshopOutput } from './models/getWorkshopOutput';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { PurchaseModalComponent } from '../purchase-modal/purchase-modal.component';
import { GetWorkshopCommentListOutput } from './models/GetWorkshopCommentListOutput';
import { CommentForm } from "../posts/models/CommentForm";
import { MessageType } from '../core/model/messageType';
import { LoginComponent } from '../login/login.component';
import { Subscription } from 'rxjs';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { FileSaverService } from 'ngx-filesaver';
import { User } from '../login/model/User';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-workshop-detail',
  templateUrl: './workshop-detail.component.html'
})
export class WorkshopDetailComponent extends BasePage implements OnInit {
  workshopIdentity: string;
  urlTitle: string;
  workshop: GetWorkshopOutput = new GetWorkshopOutput;
  workshopGuid: string;
  modalRef: NgbModalRef;
  isRegistered = false;
  page: number = 1;
  serverTotalpages: number;
  serverCurrentPageNumber: number;
  comments: Array<GetWorkshopCommentListOutput> = [];
  replyTo: GetWorkshopCommentListOutput;
  commentForm: FormGroup;
  CommentFormModel: CommentForm;
  isLogin: boolean = false;
  user: User;

  constructor(protected baseService: BaseService,
    protected toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private authService: AuthService,
    private _FileSaverService: FileSaverService

  ) {
    super(baseService, toastr);
    this.user = authService.isLogin();
    this.isLogin = this.user && this.user.userId!="" ? true : false;

    this.authService.user.subscribe(userDetail => {
      if (userDetail && userDetail.userId == "") {
        
        this.user = null;
        this.isLogin = false;
      } else if (userDetail && userDetail.userId != "") {
        this.user = userDetail;
        console.log(22, userDetail)
        this.isLogin = true;


      }
    })
  }
  ngOnInit() {
    super.ngOnInit();

    this.commentForm = this.fb.group({
      text: ['', Validators.required]
    })

    this.getWorkshop();
  }





  getWorkshop() {
    this.route.params.subscribe(params => {

      this.workshopIdentity = params.id;
      //this.urlTitle = params.urlTitle;
      this.getItem<GetWorkshopOutput>("workshop/getWorkshop/" + this.workshopIdentity).subscribe(x => {
        this.workshop = x;
        console.log(x);
        this.workshopGuid = x.id;
        //this.isRegistered = this.authService.isWorkshopRegisterd(this.workshopGuid);
        this.getComments();
      })
    });
  }

  getComments() {
    this.getListWithPagination<GetWorkshopCommentListOutput>("workshop/GetWorkshopCommentList/" + this.workshopGuid + "/" + this.page)
      .subscribe(x => {
        this.serverCurrentPageNumber = x.pageNumber;
        this.comments = this.comments.concat(x.results);
        // this.comments.forEach(comment => {
        //   if (comment.replays && comment.replays.length > 0) {
        //     comment.firstReply = comment.replays[0];
        //     comment.replays.splice(0, 1);
        //   }

        // });
        // console.log(this.comments)

      })
  }

  nextPage() {
    this.page++;
    this.getComments();
  }

  onRegister() {
    if (!this.isLogin) {

      this.modalRef = this.modalService.open(LoginComponent);
      this.modalRef.componentInstance.name = 'World';
      return;
    }
    this.getItem<{ result: boolean, text: string }>("workshop/isWorkshopAvailable/" + this.workshop.identity).subscribe(result => {
      if (!result.result) {
        let confiem = this.modalService.open(ConfirmModalComponent);
        confiem.componentInstance.title = result.text;
        confiem.componentInstance.btns = [{ text: "بله", action: true }, { text: "انصراف", action: false }];

        confiem.result.then((data: boolean) => {
          if (!data) return;
          this.modalRef = this.modalService.open(PurchaseModalComponent);
          this.modalRef.componentInstance.workshop = this.workshop;
        });
      } else {
        this.modalRef = this.modalService.open(PurchaseModalComponent);
        this.modalRef.componentInstance.workshop = this.workshop;
      }
    });


  }


  onFormSubmit() {
    if (this.commentForm.valid) {

      this.CommentFormModel = this.commentForm.value;

      if (this.replyTo) {
        this.CommentFormModel.parentId = this.replyTo.id
      }
      this.CommentFormModel.id = this.workshopGuid;

      this.postItem<GetWorkshopCommentListOutput>("workshop/AddWorkShopComment", this.CommentFormModel).subscribe(x => {
        if (x.id) {
          this.commentForm.reset();
          this.showMessage("با موفقیت ثبت شد.", MessageType.Success, 2000);
          if (this.replyTo) {
            this.comments.forEach(comment => {
              if (comment.id == this.replyTo.id || comment.children.find(x => x.id == this.replyTo.id)) {
                comment.children.push(x);
              }
            });
            this.replyTo = null;
          } else {
            x.children = [];
            this.comments.unshift(x);
          }
        }
      })
    }
  }
  scroll(el) {
    el.scrollIntoView();
  }


  private downloadBlobs: Array<{
    item: any
    url: string;
  }> = [];
  private downloadsSubscriptions: Array<{
    item: any,
    subscribe: Subscription;
  }> = [];


  cancelDownload(item: any) {
    item.percent = null;
    this.unsubItem(item);
  }


  download(item: GetWorkshopOutput): Promise<string> {
    return new Promise((resolve, reject) => {
      const findedBlob = this.downloadBlobs.find(x => x.item.id === item.id);
      if (findedBlob) {
        resolve(findedBlob.url);
        return;
      }

      this.downloadsSubscriptions.push({
        item,
        subscribe: this.baseService.download(item).subscribe(event => {
          if (event.type === HttpEventType.DownloadProgress) {
            item.downloadPercent = Math.round((100 * event.loaded) / event.total);
          } else if (event instanceof HttpResponse) {
            this._FileSaverService.save(event.body as any, item.downloadFileName);

            const blob = new Blob([event.body as any], { type: "video/mp4" });
            let url = window.URL.createObjectURL(blob);

            this.downloadBlobs.push({ item, url: url });

            resolve(url);
          }
        })
      });
    });
  }

  ngOnDestroy() {
    this.unsubAllDownloads();
  }
  unsubItem(item: any) {
    this.downloadsSubscriptions.forEach(x => {
      if (x.item.id == item.id && x.subscribe) {
        x.subscribe.unsubscribe();
      }
    });
  }
  unsubAllDownloads() {
    this.downloadsSubscriptions.forEach(x => {
      if (x.subscribe) {
        x.subscribe.unsubscribe();
      }
    });
  }
}
