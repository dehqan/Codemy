import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BasePage } from '../core/basePage';
import { BaseService, HttpType } from '../core/service/base.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { GetPostOutput } from './models/GetPostOutput';
import { GetPostCommentListItemOutput } from './models/getPostCommentListItemOutput';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { MessageType } from '../core/model/messageType';
import { AuthService } from '../shared/auth.service';
import { CommentForm } from './models/CommentForm';
import { User } from './../login/model/User';

@Component({
  selector: 'app-post',
  templateUrl: 'post-detail.component.html'
})
export class PostDetailComponent extends BasePage implements OnInit {

  replyTo: GetPostCommentListItemOutput;

  postIdentity: string;
  postGuid: string;

  urlTitle: string;
  post: GetPostOutput;
  comments: Array<GetPostCommentListItemOutput> = [];
  page: number = 1;
  serverTotalpages: number;
  serverCurrentPageNumber: number;
  commentForm: FormGroup;
  CommentFormModel: CommentForm;
  isLogin: boolean = false;
  user: User;


  constructor(protected baseService: BaseService,
    protected toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private authService: AuthService,
    private changed:ChangeDetectorRef
  ) {
    super(baseService, toastr);
    this.user = authService.isLogin();
    this.isLogin = this.user.userId != "" ? true : false;

    //   this.authService.user.subscribe(userDetail => {
    //     if (userDetail && userDetail.userId == "") {

    //       this.isLogin=false;
    //     } else if (userDetail && userDetail.userId != "") {
    //         this.isLogin=true;


    //       }
    //   })
  }



  ngOnInit(): void {
    super.ngOnInit();
    this.commentForm = this.fb.group({
      text: ['', Validators.required]
    })


    this.route.params.subscribe(params => {
      this.postIdentity = params.id;
      this.urlTitle = params.urlTitle;
      this.getItem<GetPostOutput>("post/getpost/" + this.postIdentity + "/" + this.urlTitle).subscribe(x => {
        this.post = x;
        this.postGuid = x.id;
        this.getComments();
      })
    });


  }

  getComments() {
    this.getListWithPagination<GetPostCommentListItemOutput>("post/getpostcommentlist/" + this.postGuid + "/" + this.page)
      .subscribe(x => {
        this.serverCurrentPageNumber = x.pageNumber;
        this.comments = this.comments.concat(x.results);
        // this.comments.forEach(comment => {
        //   if (comment.children && comment.children.length > 0) {
        //     comment.firstReply = comment.children[0];
        //     comment.children.splice(0, 1);
        //   }

        // });
        console.log(this.comments)

      })
  }

  nextPage() {
    this.page++;
    this.getComments();
  }

  onFormSubmit() {
    if (this.commentForm.valid) {

      this.CommentFormModel = this.commentForm.value;

      if (this.replyTo) {
        this.CommentFormModel.parentId = this.replyTo.id
      }
      this.CommentFormModel.id = this.postGuid;

      this.postItem<GetPostCommentListItemOutput>("post/addpostcomment", this.CommentFormModel).subscribe(x => {
        if (x.id) {
          this.commentForm.reset();
          this.showMessage("با موفقیت ثبت شد.", MessageType.Success, 2000);
          if (this.replyTo) {
            this.comments.forEach(comment => {
              if (comment.id == this.replyTo.id || comment.children.find(x => x.id == this.replyTo.id)) {
                comment.children.push(x);
              }
            });
            this.replyTo = null;
          } else {
            x.children = [];
            this.comments.unshift(x);
          }
        }
      })
    }
  }
  scroll(el) {
    el.scrollIntoView();
  }

}


