import { Component, OnInit } from '@angular/core';
import { BasePage } from '../core/basePage';
import { BaseService, HttpType } from '../core/service/base.service';
import { PaginationOutput } from '../core/model/defaultOutput';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { GetPostListItemOutput } from './models/GetPostListItemOutput';
import { AuthService } from '../shared/auth.service';

@Component({
  selector: 'app-posts',
  templateUrl: 'posts.component.html'
})
export class PostsComponent extends BasePage implements OnInit {

  isLogin:boolean;
  constructor(protected baseService: BaseService, protected toastr: ToastrService, private router: Router) {
    super(baseService, toastr);
  }


  news: Array<GetPostListItemOutput> = [];
  topNews: GetPostListItemOutput;
  
  page: number = 1;
  serverTotalpages: number;
  serverCurrentPageNumber: number;

  PageApi: string = "";

  ngOnInit(): void {
    super.ngOnInit();

    this.PageApi = this.router.url.indexOf('blog') > 0 ? "getbloglist" : "getnewslist";

    this.getPosts();

  }

  nextPage() {
    this.page++;
    this.getPosts();
  }

  getPosts() {
    this.getListWithPagination<GetPostListItemOutput>("post/"+this.PageApi+"/" + this.page)
      .subscribe(x => {
        this.serverCurrentPageNumber = x.pageNumber;
        this.news = this.news.concat(x.results);

        if (this.page == 1 && this.news.length > 0) {
          this.serverTotalpages = x.totalNumberOfPages;
          this.topNews = this.news[0];
          this.news.splice(0, 1);
        }
      });
  }
}
