import { BaseVm } from "../../core/model/baseModel";

export class GetPostOutput extends BaseVm {
    title: string;
    urlTitle: string;
    postContent: string;
    summary: string;
image:string;
    category: Category_GetPostOutput;
    postType: PostTypeEnum
    tags: Array<Tag_GetPostOutput>;
}

export class Category_GetPostOutput {
    title: string;
    urlTitle: string;
}

export class Tag_GetPostOutput {
    title: string;
    urlTitle: string;
}

export enum PostTypeEnum {
    None = 0,
    Blog = 1,
    News = 2
}