import { BaseVm } from "../../core/model/baseModel";

export class GetPostListItemOutput extends BaseVm{
    title:string;
    summary:string;
    urlTitle:string
    category:string;
    image:string;
}