import { BaseVm } from "../../core/model/baseModel";

export class GetPostCommentListItemOutput extends BaseVm{
    text:string;
    Rate:number;
    username:number;
    children:Array<GetPostCommentListItemOutput>;
    
    firstReply?:GetPostCommentListItemOutput;
    inPage:number;


}

