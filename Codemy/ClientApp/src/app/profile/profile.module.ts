import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../shared/shared.module';


@NgModule({
  imports: [ RouterModule, SharedModule ],
  declarations: [ ],
  exports: [ ],
  providers: [ ]
})
export class ProfileModule { };
