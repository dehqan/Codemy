import { BaseVm } from "../../core/model/baseModel";

export class GetRegisteredWorkshopListOutput extends BaseVm {
    title: string;
    cost: number;
    status: number;
    holdingDateTime: string;
    isPaid: boolean;
}