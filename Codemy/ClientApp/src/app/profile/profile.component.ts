import { Component, OnInit, Input } from '@angular/core';
import { BasePage } from '../core/basePage';
import { BaseService, HttpType } from '../core/service/base.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbdModalContent } from './ProfileModalContent';
import { GetRegisteredWorkshopListOutput } from './models/GetRegisteredWorkshopListOutput';
import { AuthService } from '../shared/auth.service';
import { User } from '../login/model/User';

@Component({
  selector: 'app-profile',
  templateUrl: 'profile.component.html'
})
export class ProfileComponent extends BasePage implements OnInit {


  page: number = 1;
  serverTotalpages: number;
  serverCurrentPageNumber: number;
  workshops: Array<GetRegisteredWorkshopListOutput> = [];
  isLogin: boolean;
  user: User;

  constructor(private modalService: NgbModal,
    protected baseService: BaseService,
    protected toastr: ToastrService,
    private router: Router,
    private authService: AuthService
  ) {
    super(baseService, toastr);
    this.user = authService.isLogin();
    this.authService.user.subscribe(userDetail => {
      if (userDetail && userDetail.userId == "") {
        this.user = null;
        this.isLogin = false;
      } else if (userDetail && userDetail.userId != "") {
        this.user = userDetail;
        this.isLogin = true;


      }
    })
  }



  ngOnInit(): void {
    super.ngOnInit();
    this.getWorkshopList();
  }

  nextPage() {
    this.page++;
    this.getWorkshopList();
  }

  open() {
    const modalRef = this.modalService.open(NgbdModalContent);
    modalRef.componentInstance.name = 'World';
  }

  getWorkshopList() {
    this.getListWithPagination<GetRegisteredWorkshopListOutput>("order/getRegisteredWorkshopList/" + this.page)
      .subscribe(x => {
        this.serverCurrentPageNumber = x.pageNumber;
        this.workshops = this.workshops.concat(x.results);

        if (this.page == 1 && this.workshops.length > 0) {
          this.serverTotalpages = x.totalNumberOfPages;
        }
      });
  }

}

