import {Component, Input} from '@angular/core';

import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'ngbd-modal-content',
  template: `
  <div class="modal-header">
  <h4 class="modal-title">Hi there!</h4>
  <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="modal-body">
<section class="profile-edit">
<div class="pro-edit-wrapper">
  <div class="profile">
    <a class="icon-img">
      
    </a>
    <a class="profile-img">
      <img src="assets/images/profile-img.jpg" alt="">
    </a>
  </div>
  <div class="pro-form">
    <form>
      <input type="text" name="name" placeholder="نام">
      <input type="text" name="last-name" placeholder="نام خانوادگی">
      <input type="text" name="email" placeholder="ایمیل">
      <input type="text" name="number" placeholder="شماره تماس ">
      <span>
        *ایمیل و شماره تماس شما به هیچ وجه برای مقاصدی جز اطلاع رسانی هرچه سریع تر و دسترسی ساده تر به شما استفاده نمیشود.
      </span>
      <button class="btn" type="submit" value="Submit">ذخیره تغییرات</button>
    </form> 
  </div>
</div>
</section>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-outline-dark" (click)="activeModal.close('Close click')">Close</button>
</div>
	
  `
})
export class NgbdModalContent {
  @Input() name;

  constructor(public activeModal: NgbActiveModal) {}
}
