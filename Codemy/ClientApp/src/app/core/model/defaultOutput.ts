export class DefaultOutputResult<T> {
    status: number;
    error: string;
    result: T;
}

export class PaginationOutput<T>{
    pageNumber : number;
    pageSize : number;
    totalNumberOfPages : number;
    totalNumberOfrds : number;
    nextPageUrlnumber:number;
    results : Array<T>;
}