export enum MessageType {
    Success = 1,
    Error = 0,
    Info = 2,
    Warning = 3
}