
export class ProfileData {
    UserId: number;
    IsGuest: boolean;
    IsLogin: boolean;
    UserName: string;
    FullName: string;
    Avatar: string;
    Email: string;
    IsRegistrationSession: boolean;
    AvatarSkip: boolean;
    Roles: Array<RoleModel>
    RoleType: number;
    PublisherId: number;
    FirstName: string;
    LastName: string;
    Token:string;
    Pass:string;
}

export class EditProfileModel {
    UserName: string;
    FirstName: string;
    LastName: string;
}

export class RoleModel {
    RoleId: number;
}


