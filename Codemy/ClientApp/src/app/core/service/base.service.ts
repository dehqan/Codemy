import { RequestOptionsArgs, Headers } from '@angular/http';
import { Observable } from "rxjs/Rx";
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { StorageService } from './storage.service';
import { LocalConstants } from '../model/global';
import { ProfileData } from '../model/profileData';
import { Dictionary } from '../model/dictionary';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { DefaultOutputResult, PaginationOutput } from '../model/defaultOutput';
import { GetWorkshopOutput } from '../../workshops/models/getWorkshopOutput';
@Injectable()

export class BaseService {

    constructor(public _http: HttpClient) {

    }


    Request<T>(url: string, type: HttpType, model?: any, headersDic?: Array<Dictionary>): Observable<DefaultOutputResult<T>> {
        const httpOptions = this.makeHttpOption(headersDic);
        if (type == HttpType.Post)
            return this._http.post<DefaultOutputResult<T>>("api/" + url, !model ? {} : model, httpOptions);
        else if (type == HttpType.Get)
            return this._http.get<DefaultOutputResult<T>>("api/" + url, httpOptions);
    }

    // ListRequest<T>(url: string, type: HttpType, model?: any, headersDic?: Array<Dictionary>): Observable<DefaultOutputResult<PaginationOutput<T>>> {
    //     const httpOptions = this.makeHttpOption(headersDic);
    //     if (type == HttpType.Post)
    //     return this._http.post<DefaultOutputResult<PaginationOutput<T>>>(url, JSON.stringify(!model ? {} : model), httpOptions);
    //     else if (type == HttpType.Get)
    //     return this._http.get<DefaultOutputResult<PaginationOutput<T>>>(url, httpOptions);

    // }

    private makeHttpOption(headersDic?: Array<Dictionary>): { headers: HttpHeaders } {
        var headers: HttpHeaders = new HttpHeaders().set('Content-Type', 'application/json');;
        var token = JSON.parse(StorageService.getLocalStorage("token"));
        if (token) {
            headers = headers.set('Authorization', 'bearer ' + token);

            if (headersDic && headersDic.length != 0)
                headersDic.forEach(item => {
                    headers.set(item.Key, item.Value);
                });
            return {
                headers: headers
            };
        }

    }

    public download(item: GetWorkshopOutput): Observable<any> {
        var token = JSON.parse(StorageService.getLocalStorage("token"));
        if (token) {
            const req = new HttpRequest("GET", item.downloadUrl, {
                reportProgress: true,
                headers: new HttpHeaders({
                    Authorization: "Bearer " + token
                }),
                observe: "response",
                responseType: "blob"
            });
            return this._http.request(req);
        }
    }
}
export enum HttpType {
    Post = 1,
    Get = 0
}
