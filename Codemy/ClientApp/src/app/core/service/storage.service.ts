import { Injectable } from '@angular/core';

@Injectable()
export class StorageService {


    static addOrUpdateLocalStorageWithSerialize(key: string, value: any) {
        if (!localStorage) return;

        var finded =  localStorage.getItem(key);
        if (finded)//update
            finded = JSON.stringify(value);
        else {//add
            localStorage.setItem(key, JSON.stringify(value));
        }
    }
    static addOrUpdateLocalStorage(key: string, value: any) {
        if (!localStorage) return;
        
        var finded =  localStorage.getItem(key);
        if (finded)//update
            finded = value;
        else {//add
            localStorage.setItem(key, value);
        }
    }

    static removeLocalStorage(key: string) {
        if (!localStorage) return;
        
        var finded = localStorage.getItem(key);
        if (finded)//remove
        localStorage.removeItem(key);
    }
    static getLocalStorage(key: string) {
        if (!localStorage) return "";
        

        return localStorage.getItem(key);
    }

    static getWindowWidth(): number {
        return window.innerWidth;
    }

}





