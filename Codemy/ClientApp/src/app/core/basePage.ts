import { BaseService, HttpType } from "./service/base.service";
import { Observable } from "rxjs/Observable";
import { map, catchError } from 'rxjs/operators'
import { retry } from "rxjs/operator/retry";
import { OnInit } from "@angular/core";
import { PaginationOutput, DefaultOutputResult } from "./model/defaultOutput";
import { ToastrService, IndividualConfig } from "ngx-toastr";
import { MessageType } from "./model/messageType";

import 'rxjs/add/observable/throw';
import { HttpErrorResponse } from "@angular/common/http";

export class BasePage implements OnInit {
    ngOnInit(): void {

    }
    constructor(protected baseService: BaseService, protected toastr: ToastrService) {
    }

    protected getItem<T>(url: string): Observable<T> {
        return this.baseService.Request<T>(url, HttpType.Get).pipe(
            map(x => {
                if (x.status != 1) {
                    throw x;
                }
                return x.result
            }), catchError((err: DefaultOutputResult<any>) => {
                this.showMessage(err.error, err.status);
                return Observable.throw(err);
            }));
        // .shareReplay();//This makes shareReplay ideal for handling things like caching AJAX results, as it's retryable
    }

    protected postItem<T>(url: string, model: any): Observable<T> {
        return this.baseService.Request<T>(url, HttpType.Post, model).pipe(
            map(x => {
                if (x.status != 1) {
                    throw x;
                }
                return x.result
            }), catchError((err: DefaultOutputResult<any>) => {
                this.showMessage(err.error, err.status);
                return Observable.throw(err);
            }));
        // .shareReplay();//This makes shareReplay ideal for handling things like caching AJAX results, as it's retryable
    }


    protected getListWithPagination<T>(url: string): Observable<PaginationOutput<T>> {
        return this.baseService.Request<PaginationOutput<T>>(url, HttpType.Get).pipe(
            map(x => {

                if (x.status != 1) {
                    throw x;
                }
                return x.result
            }), catchError(err => {
                if (err instanceof HttpErrorResponse) {
                    if (err.status === 401) {
                        this.showMessage(" دسترسی ممکن نیست." + url, -1);
                        return Observable.throw(err);

                    }
                }
                this.showMessage(err.error, err.status);
                return Observable.of(new PaginationOutput<T>());
            }));
        // .shareReplay();//This makes shareReplay ideal for handling things like caching AJAX results, as it's retryable
    }


    protected showMessage(message: string, type: MessageType, timeOut?: number) {
        var config: Partial<IndividualConfig> = {
            closeButton: true,
            timeOut: timeOut ? timeOut : 100000,
            positionClass: 'toast-bottom-left'
        }
        switch (type) {
            case 1:
                this.toastr.success(message, '', config);
                break;
            case -1:
            case 0:
                this.toastr.error(message, '', config);
                break;
            case 2:
                this.toastr.info(message, '', config);
                break;
            case 3:
                this.toastr.warning(message, '', config);
                break;

            default:
                break;
        }
    }
}

