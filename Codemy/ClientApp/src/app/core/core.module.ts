import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BaseService } from './service/base.service';
import { StorageService } from './service/storage.service';


//فقط برای بخش های سینگلتون این ماژول تعبیه شده است
//این ماژول به ماژول اصلی پروژه ایمپورت باید شود و نه همه ماژول ها
//از سرویس های این ماژول یک نمونه در کل پروژه ایجاد میشود
//این ماژول به درد نگهداری یوزر جاری میخورد
@NgModule({
  imports: [ CommonModule, RouterModule ],
  exports: [ ],
  declarations: [ ],
  providers: [  BaseService,StorageService]
})
export class CoreModule { };
