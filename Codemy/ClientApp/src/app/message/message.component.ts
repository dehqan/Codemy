import { Component, OnInit } from '@angular/core';
import { BasePage } from '../core/basePage';
import { BaseService } from '../core/service/base.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-post',
    templateUrl: 'message.component.html'
})

export class MessageComponent extends BasePage implements OnInit {
    route: string;
    actionMessage: string;
    message: string;
    pw: any;
    tid: any;
    constructor(protected baseService: BaseService,
        protected toastr: ToastrService,
        private router: Router,

        private activatedRoute: ActivatedRoute

    ) {
        super(baseService, toastr);
    }

    ngOnInit() {
        this.activatedRoute.queryParams.subscribe(params => {
            this.message = params["message"];
            this.actionMessage = params["actionMessage"];
            this.route = params["route"];
            this.tid = params["tid"];
            this.pw = params["pw"];

        });
    }

    goToPage() {
        this.router.navigate([this.route])
    }
}
