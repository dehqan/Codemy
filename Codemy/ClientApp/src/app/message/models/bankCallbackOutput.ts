import { BaseVm } from "../../core/model/baseModel";

export class BankCallbackOutput  {

  order: BuyWorkShopOutput;
  token?: string;
}

export enum OrderStatusEnum {
  Paid = 1,
  NotPaid = 0

}

export class BuyWorkShopOutput extends BaseVm{
  status: OrderStatusEnum;
  sescription: string;
  transactionId: string;
  PaymentRequestOutputStatus: number;
  paymentRequestOutputAuthority: string;

  paymentRequestAsyncDescription: string;
  paymentVerificationAsyncDescription: string;

  userId: string;
  workshopIdentity: string;
}