import { BankCallbackOutput, OrderStatusEnum, BuyWorkShopOutput } from './models/bankCallbackOutput';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BasePage } from '../core/basePage';
import { ToastrService } from 'ngx-toastr';
import { BaseService } from '../core/service/base.service';
import { AuthService } from '../shared/auth.service';



@Component({
  selector: "app-landing-page",
  templateUrl: 'landing-page.component.html'
})

export class LandingPageComponent extends BasePage implements OnInit {
  route: string;
  actionMessage: string;
  message: string;

  loading: boolean = true;
  isError: boolean = false;
  status: any;
  authority: any;
  order: BuyWorkShopOutput = new BuyWorkShopOutput();
  constructor(protected baseService: BaseService,
    protected toastr: ToastrService,
    private router: Router,
    public authService: AuthService,

    private activatedRoute: ActivatedRoute

  ) {
    super(baseService, toastr);
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.authority = params["Authority"];
      this.status = params["Status"];
      if (this.authority && this.status) {

        this.getItem<BankCallbackOutput>("order/BankCallback?authority=" + this.authority + "&status=" + this.status).subscribe(result => {
          this.order = result.order;
          console.log(result);
          this.loading=false;
          if (this.order.status != OrderStatusEnum.Paid) {
            this.message = "تراکنش موفقیت آمیز نبود. : شماره پیگیری " + this.order.identity;
            this.actionMessage = "بازگشت به سایت";
            this.isError = true;
            this.route = "/home";

          } else {

            this.isError = false;
            this.message = "تراکنش موفقیت آمیز بود. شماره پیگیری : " + this.order.identity;
            this.actionMessage = "ورود به صفحه ورکشاپ";
            this.route = "/workshop/" + this.order.workshopIdentity
            this.authService.setUserDetail(result.token);

          }

        })
      }

    });
  }

  goToPage() {
    this.router.navigate([this.route])
  }
}
