import { AuthService } from './auth.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoadingSpinnerComponent } from './loading-spinner.component';
import { StorageService } from './storage.service';

//این ماژول در ماژول های دیگر به جز ماژول اصلی ایمپورت میشود
//این ماژول حاوی فیچر هایی است که در هر جای دیگر پروژه ممکن است استفاده شود
//توست، لودینگ، مدال و چیزهایی از این دست در این ماژول تعریف میشوند

@NgModule({
  imports: [ CommonModule ],
  //declarations: [ LoadingSpinnerComponent ],
  //exports: [ LoadingSpinnerComponent, CommonModule ],
  providers: [ StorageService,AuthService]
})
export class SharedModule { };
