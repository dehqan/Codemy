export class StorageService {
    static set(key: string, value: any, notSerialize?: boolean) {
        if (notSerialize)
            localStorage.setItem(key, <any>value);
        else
            localStorage.setItem(key, JSON.stringify(value));


    }
    static removeLocalStorage(key: string) {
        var finded = localStorage.getItem(key);
        if (finded)//remove
            localStorage.removeItem(key);
    }
    static Get<T>(key: string, notDeserilize?: boolean): T {
        if (notDeserilize)
            return <any>localStorage.getItem(key);
        else
            return <T>JSON.parse(localStorage.getItem(key))

    }
    static removeAllLocalStorage() {
        localStorage.clear();
    }
}