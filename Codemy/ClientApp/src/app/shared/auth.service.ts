import { Injectable } from "@angular/core";
import { StorageService } from "./storage.service";
import { Subject, BehaviorSubject } from "rxjs";
import { extend } from "../../../node_modules/webdriver-js-extender";
import { BaseService } from "../core/service/base.service";
import { JwtHelperService } from '@auth0/angular-jwt';
import { User } from "../login/model/User";

@Injectable()
export class AuthService extends BaseService {
    public user: BehaviorSubject<User> = new BehaviorSubject(null);


    public setUserDetail(token: string) {
        StorageService.set("token", token);
        this.user.next(this.decodeToken()) // add this!

    }
    public clearUserDetail() {
        StorageService.removeLocalStorage("token");
        this.user.next({
            avatar: "",
            email: "",
            fullName: "",
            userId: "",
            userName: ""
        }) 

    }

    public isLogin(): User {
        var token = StorageService.Get<string>("token");
        if (token) {
            var user = this.decodeToken()
           // this.user.next(user)
            return user;
        }
        return {
            avatar: "",
            email: "",
            fullName: "",
            userId: "",
            userName: ""
        };

    }

    isWorkshopRegisterd(workshopId: string) {
        const helper = new JwtHelperService();
        var token = StorageService.Get<string>("token");

        const decodedToken = helper.decodeToken(token);
        if (decodedToken && decodedToken.Workshops) {
            var ids = decodedToken.Workshops;
            let finded = ids.indexOf(workshopId);
            if (finded>=0) return true;

        }
        return false;

    }

    decodeToken(): User {
        const helper = new JwtHelperService();
        var  decodedToken= StorageService.Get<string>("token");
        const  user= helper.decodeToken(decodedToken);
        return user;
    }


}
