import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '../../../node_modules/@ng-bootstrap/ng-bootstrap';
import { BasePage } from '../core/basePage';
import { BaseService } from '../core/service/base.service';
import { ToastrService } from '../../../node_modules/ngx-toastr';
import { GetWorkshopOutput } from '../workshops/models/getWorkshopOutput';
import { NavigationExtras, Router } from '@angular/router';
import { BuyWorkShopOutput } from '../message/models/bankCallbackOutput';
import { AuthService } from '../shared/auth.service';
import { GetPostCommentListItemOutput } from '../posts/models/getPostCommentListItemOutput';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html'
})
export class CommentsComponent extends BasePage implements OnInit {

@Input()
  comments:Array<GetPostCommentListItemOutput> = [];
  ngOnInit() {
  }


  constructor(protected baseService: BaseService,
    protected toastr: ToastrService,

  ) {
    super(baseService, toastr);
  }



}
