import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { CoreModule } from './core/core.module';
import { BaseService } from './core/service/base.service';
import { CoursesComponent } from './courses/courses.component';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import { LoadingBarModule } from '@ngx-loading-bar/core'
import { PostsComponent } from './posts/posts.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { HeaderComponent } from './header.component';
import { PostDetailComponent } from './posts/post-detail.component';
import { FooterComponent } from './footer.component';
import { BannerComponent } from './banner.component';

import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { WorkshopsComponent } from './workshops/workshops.component';
import { WorkshopDetailComponent } from './workshops/workshop-detail.component';
import { ProfileComponent } from './profile/profile.component';
import {NgbModule, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { NgbdModalContent } from './profile/ProfileModalContent';
import { LoginComponent } from './login/login.component';
import { AuthService } from './shared/auth.service';
import { PurchaseModalComponent } from './purchase-modal/purchase-modal.component';
import { MessageComponent } from './message/message.component';
import { LandingPageComponent } from './message/landing-page.component';
import { FileSaverModule } from 'ngx-filesaver';
import { GravatarModule } from  'ngx-gravatar';
import { ConfirmModalComponent } from './confirm-modal/confirm-modal.component';
import { CommentsComponent } from './comments/comments.component';

@NgModule({

  imports: [
    NgbModule.forRoot(),
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    LoadingBarHttpClientModule,
    LoadingBarRouterModule,
    LoadingBarModule.forRoot(),
    CoreModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(), // ToastrModule added
    ScrollToModule.forRoot(),
    FileSaverModule,
    GravatarModule,

    RouterModule.forRoot([
      { path: '', component:HomeComponent },
      { path: 'courses', component:CoursesComponent },
      { path: 'courses/:id', component:CoursesComponent },

      { path: 'news', component:PostsComponent },
      { path: 'blog', component:PostsComponent },
      { path: 'post/:id/:urlTitle', component:PostDetailComponent },
      { path: 'workshops', component:WorkshopsComponent },
      { path: 'workshop/:id', component:WorkshopDetailComponent },
      { path: 'profile', component:ProfileComponent },
      { path: 'login', component:LoginComponent },
      { path: 'message', component:MessageComponent },
      { path: 'landingpage', component:LandingPageComponent },

      //{ path: 'home', loadChildren: 'app/home/home.module#HomeModule' }
    ])
  ],
  declarations: [
    HomeComponent,
    AppComponent,
    CoursesComponent,
    PostsComponent,
    PostDetailComponent,
    HeaderComponent,
    FooterComponent,
    BannerComponent,
    WorkshopsComponent,
    WorkshopDetailComponent,
    ProfileComponent,
    NgbdModalContent,
    LoginComponent,
    PurchaseModalComponent,
    MessageComponent,
    LandingPageComponent,
    ConfirmModalComponent,
    CommentsComponent

  ],
  providers: [BaseService, NgbActiveModal,AuthService],
  bootstrap: [AppComponent],
  entryComponents:[NgbdModalContent, LoginComponent,PurchaseModalComponent,
    ConfirmModalComponent
  ]
})
export class AppModule { }
