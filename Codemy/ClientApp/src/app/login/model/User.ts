import { BaseVm } from "../../core/model/baseModel";

export class User {
    userId :string;
    userName :string;
    fullName:string;
    avatar :string;
    email :string;

}