import { StorageService } from './../shared/storage.service';
import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { NgbActiveModal } from '../../../node_modules/@ng-bootstrap/ng-bootstrap';
import { BasePage } from '../core/basePage';
import { BaseService } from '../core/service/base.service';
import { ToastrService } from '../../../node_modules/ngx-toastr';

import { AuthService } from '../shared/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent extends BasePage implements OnInit {

  PhoneForm: FormGroup;
  CodeForm: FormGroup;
  phoneSubmitted: boolean = false;
  codeSubmitted: boolean = false;
  showCodeForm: boolean = false;
  @Input() name;

  get pf() { return this.PhoneForm.controls; }
  get cf() { return this.CodeForm.controls; }


  ngOnInit() {
    this.PhoneForm = new FormGroup({
      'mobile': new FormControl(null, [Validators.required]),

    });

    this.CodeForm = new FormGroup({
      // 'mobile': new FormControl(null, [Validators.required]),
      'password': new FormControl(null, [Validators.required]),

    });
  }


  constructor(protected baseService: BaseService,
    protected toastr: ToastrService,
    public activeModal: NgbActiveModal,
    public authService: AuthService
  ) {
    super(baseService, toastr);
  }


  onPhoneSubmit() {
    this.phoneSubmitted = true;
    if (!this.PhoneForm.valid) return;
    this.postItem<any>("Security/Register", { username: this.PhoneForm.value.mobile })
      .subscribe(x => {
        this.showCodeForm = true;
      }, err => console.log(err));
  }

  onCodeSubmit() {
    this.codeSubmitted = true;

    this.postItem<string>("Security/Login", { username: this.PhoneForm.value.mobile, password: this.CodeForm.value.password })
      .subscribe(token => {
        this.authService.setUserDetail(token);
        var test=this.authService.decodeToken();
        this.close();
      });
  }

  retry() {
    this.PhoneForm.markAsPristine();
    this.PhoneForm.markAsUntouched();
    this.PhoneForm.updateValueAndValidity();
    this.PhoneForm.reset();
    this.showCodeForm = false;
  }

  close(){
    this.activeModal.dismiss();
  }
}
