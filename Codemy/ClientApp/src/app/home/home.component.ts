import { Component, OnInit } from '@angular/core';
import { BaseService } from '../core/service/base.service';
import { GetWorkshopListOutput } from './model/getWorkshopListOutput';
import { DefaultOutputResult, PaginationOutput } from '../core/model/defaultOutput';
import { BasePage } from '../core/basePage';
import { ToastrService } from 'ngx-toastr';
import { GetPostListItemOutput } from '../posts/models/GetPostListItemOutput';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent extends BasePage implements OnInit {

  workshops: Array<GetWorkshopListOutput> = [];
  news: Array<GetPostListItemOutput> = [];

  constructor(protected baseService: BaseService, protected toastr: ToastrService) {
    super(baseService,toastr);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.getListWithPagination<GetWorkshopListOutput>("workshop/gethomeworkshoplist").subscribe(x => { this.workshops = x.results; });
    this.getListWithPagination<GetPostListItemOutput>("post/getnewslist/1").subscribe(x => { this.news = x.results; });

  }



}
