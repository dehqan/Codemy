import { AuthService } from './shared/auth.service';
import { Component, OnInit } from '@angular/core';
import { BasePage } from './core/basePage';
import { NgbModal, NgbModalRef } from '../../node_modules/@ng-bootstrap/ng-bootstrap';
import { BaseService } from './core/service/base.service';
import { ToastrService } from '../../node_modules/ngx-toastr';
import { LoginComponent } from './login/login.component';
import { Router } from '../../node_modules/@angular/router';
import { MessageType } from './core/model/messageType';
import { User } from './login/model/User';

@Component({
  selector: 'app-header',
  templateUrl: 'header.component.html',
})
export class HeaderComponent extends BasePage implements OnInit {

  modalRef: NgbModalRef;
  userDetail: User;
  constructor(private modalService: NgbModal,
    protected baseService: BaseService,
    protected toastr: ToastrService,
    private router: Router,
    private authService: AuthService
  ) {
    super(baseService, toastr);
  }



  ngOnInit(): void {
    super.ngOnInit();
    this.userDetail=this.authService.isLogin();
    this.authService.user.subscribe(userDetail => {
      if (userDetail && userDetail.userId == "") {
        this.userDetail=null;
      } else if (userDetail && userDetail.userId != "") {
          this.userDetail = userDetail;
          if (this.modalRef)
            this.modalRef.dismiss();

        }
    })

  }

  open() {
    this.modalRef = this.modalService.open(LoginComponent);
    this.modalRef.componentInstance.name = 'World';
  }

  
  signout() {
    this.showMessage("شما از اکانت خود خارج شدید", MessageType.Info, 3000)

    this.authService.clearUserDetail();
    this.userDetail=this.authService.isLogin();

  }
}