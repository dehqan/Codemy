import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '../../../node_modules/@ng-bootstrap/ng-bootstrap';
import { BasePage } from '../core/basePage';
import { BaseService } from '../core/service/base.service';
import { ToastrService } from '../../../node_modules/ngx-toastr';
import { GetWorkshopOutput } from '../workshops/models/getWorkshopOutput';
import { NavigationExtras, Router } from '@angular/router';
import { BuyWorkShopOutput } from '../message/models/bankCallbackOutput';
import { AuthService } from '../shared/auth.service';

@Component({
  selector: 'app-purchase',
  templateUrl: './purchase-modal.component.html'
})
export class PurchaseModalComponent extends BasePage implements OnInit {

  @Input() workshop: GetWorkshopOutput;



  ngOnInit() {
  }


  constructor(protected baseService: BaseService,
    protected toastr: ToastrService,
    public activeModal: NgbActiveModal,
    public router:Router,
    public authService: AuthService,

  ) {
    super(baseService, toastr);
  }

  goToBank() {
    this.getItem<{url:string,order:BuyWorkShopOutput, token?: string}>("order/buyworkshop/" + this.workshop.identity).subscribe(result => {
      if (result.url) {
        if (result.url != "") {
          window.location.href = result.url;
          return;
        }
        //this.authService.setUserDetail(result.token);
        let navigationExtras: NavigationExtras = {
          queryParams: {
            "message": "ثبت نام با موفقیت انجام شد.",
            "actionMessage": "برای انتقال به صفحه کلاس کلیک کنید.",
            "route": "workshop/" +this.workshop.identity,
          }
        };
        this.router.navigate(["message"], navigationExtras);
      }
    })
  }
  


}
