import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '../../../node_modules/@ng-bootstrap/ng-bootstrap';
import { BasePage } from '../core/basePage';
import { BaseService } from '../core/service/base.service';
import { ToastrService } from '../../../node_modules/ngx-toastr';
import { GetWorkshopOutput } from '../workshops/models/getWorkshopOutput';
import { NavigationExtras, Router } from '@angular/router';
import { BuyWorkShopOutput } from '../message/models/bankCallbackOutput';
import { AuthService } from '../shared/auth.service';

@Component({
  selector: 'app-cnfirm',
  templateUrl: './confirm-modal.component.html'
})
export class ConfirmModalComponent extends BasePage implements OnInit {

  @Input() title: string;
  @Input() btns: { text: string, action: boolean }[];



  ngOnInit() {
  }


  constructor(protected baseService: BaseService,
    protected toastr: ToastrService,
    public activeModal: NgbActiveModal,
    public router: Router,
    public authService: AuthService,

  ) {
    super(baseService, toastr);
  }

  onClick(btn: { text: string, action: boolean }) {
    this.activeModal.close(btn.action)
  }



}
