using System;

namespace Codemy.Domain.IRepository
{
  public interface IUnitOfWork : IDisposable
  {
    void Save();

    IPostRepository PostRepository { get; }
    IOrderRepository OrderRepository { get; }
    ICategoryRepository CategoryRepository { get; }
    IWorkshopRepository WorkshopRepository { get; }
    IWorkshopFileRepository WorkshopFileRepository { get; }
    IDownloadLinkRepository DownloadLinkRepository { get; }
    IWorkshopCommentRepository WorkshopCommentRepository { get; }
    IWorkshopRateRepository WorkshopRateRepository { get; }
    IPostCommentRepository PostCommentRepository { get; }
    ISmsLogRepository SmsLogRepository { get; }
    IContactRepository ContactRepository { get; }
    ICourseRepository CourseRepository { get;  }
  }
}
