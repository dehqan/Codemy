using Codemy.Domain.Entity;

namespace Codemy.Domain.IRepository
{
  public interface IPostRepository : IRepository<Post> { }
  public interface IOrderRepository : IRepository<Order> { }
  public interface ICategoryRepository : IRepository<Category> { }
  public interface IWorkshopRepository : IRepository<Workshop> { }
  public interface IWorkshopFileRepository : IRepository<WorkshopFile> { }
  public interface IDownloadLinkRepository : IRepository<DownloadLink> { }
  public interface IWorkshopCommentRepository : IRepository<WorkshopComment> { }
  public interface IWorkshopRateRepository : IRepository<WorkshopRate> { }
  public interface IPostCommentRepository : IRepository<PostComment> { }
  public interface ISmsLogRepository : IRepository<SmsLog> { }
  public interface IContactRepository : IRepository<Contact> { }
  public interface ICourseRepository : IRepository<Course> { }

}
