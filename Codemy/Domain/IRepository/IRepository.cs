using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Codemy.Domain.IRepository
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> AsQueryable();
        ICollection<T> GetAll();
        Task<ICollection<T>> GetAllAsync();
        T Get(int id);
        Task<T> GetAsync(int id);
        T Add(T model);
        T Find(Expression<Func<T, bool>> match);
        Task<T> FindAsync(Expression<Func<T, bool>> match);
        ICollection<T> FindAll(Expression<Func<T, bool>> match);
        Task<ICollection<T>> FindAllAsync(Expression<Func<T, bool>> match);
        IQueryable<T> FindBy(Expression<Func<T, bool>> predicate);
        Task<ICollection<T>> FindByAsync(Expression<Func<T, bool>> predicate);
        int Count();
        Task<int> CountAsync();
        IQueryable<T> GetAllIncluding(params Expression<Func<T, object>>[] includeProperties);
    }
}
