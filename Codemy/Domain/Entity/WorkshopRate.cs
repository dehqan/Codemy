using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Codemy.Domain.Entity
{
    public class WorkshopRate : BaseEntity
    {
        public int Rate { get; set; }

        [ForeignKey("UserId")]
        public AppUser UserDetail { get; set; }
        public string UserId { get; set; }

        [ForeignKey("WorkshopId")]
        public Workshop Workshop { get; set; }
        public string WorkshopId { get; set; }
    }
}
