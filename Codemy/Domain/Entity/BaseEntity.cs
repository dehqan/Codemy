using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Codemy.Domain.Entity
{
    public class BaseEntity
    {
        public BaseEntity()
        {
            Id = Guid.NewGuid().ToString();
            IsDeleted = false;
            CreateDateTime = DateTime.Now;
        }

        public string Id { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Identity { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime CreateDateTime { get; set; }


    }
}
