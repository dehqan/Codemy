using System.ComponentModel.DataAnnotations.Schema;

namespace Codemy.Domain.Entity
{
    public class PostTag : BaseEntity
    {
        [ForeignKey("PostId")]
        public Post Post { get; set; }
        public string PostId { get; set; }


        [ForeignKey("TagId")]
        public Tag Tag { get; set; }
        public string TagId { get; set; }
    }
}
