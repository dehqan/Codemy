using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace Codemy.Domain.Entity
{
  public class AppUser : IdentityUser
  {
    public AppUser()
    {
      IsDeleted = false;
      CreateDateTime = DateTime.Now;
    }
    public DateTime? LastRegisterationDateTime { get; set; }

    [MaxLength(50)]
    public string FirstName { get; set; }

    [MaxLength(50)]
    public string LastName { get; set; }

    [MaxLength(20)]
    public string Mobile { get; set; }

    [MaxLength(500)]
    public string Avatar { get; set; }

    [MaxLength(2000)]
    public string Description { get; set; }


        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public long Identity { get; set; }
    
    public bool IsDeleted { get; set; }

    public DateTime CreateDateTime { get; set; }

    public bool IsProfileCompleted { get; set; }

    public ICollection<WorkshopComment> CourseComments { get; set; }
    public ICollection<PostComment> PostComments { get; set; }
    public ICollection<Workshop> Workshops { get; set; }
    public ICollection<Order> Orders { get; set; }
    public ICollection<DownloadLink> DownloadLinks { get; set; }
    public ICollection<Contact> Contacts { get; set; }
  }
}
