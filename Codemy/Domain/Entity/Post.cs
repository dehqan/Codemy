using Codemy.Domain.Enum;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Codemy.Domain.Entity
{
  public class Post : BaseEntity
  {
    public string Title { get; set; }
    public string PostContent { get; set; }
    public string Summary { get; set; }
    public string UrlTitle { get; set; }
    public string Image { get; set; }


    public bool IsSpecial { get; set; }

    [ForeignKey("CategoryId")]
    public Category Category { get; set; }
    public string CategoryId { get; set; }
    public PostTypeEnum PostType { get; set; }


    public ICollection<PostTag> PostTags { get; set; }

    public ICollection<PostComment> PostComments { get; set; }
  }
}
