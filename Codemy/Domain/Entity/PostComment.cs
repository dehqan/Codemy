using System.ComponentModel.DataAnnotations.Schema;

namespace Codemy.Domain.Entity
{
  public class PostComment : BaseEntity
  {
    public string Text { get; set; }
    public decimal Rate { get; set; }

    [ForeignKey("PostId")]
    public Post Post { get; set; }
    public string PostId { get; set; }

    [ForeignKey("UserId")]
    public AppUser UserDetail { get; set; }
    public string UserId { get; set; }

    [ForeignKey("ParentId")]
    public PostComment Parent { get; set; }
    public string ParentId { get; set; }



  }
}
