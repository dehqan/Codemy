using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Codemy.Domain.Enum;

namespace Codemy.Domain.Entity
{
    public class SmsLog : BaseEntity
    {
        [ForeignKey("UserId")]
        public AppUser UserDetail { get; set; }
        public string UserId { get; set; }

        [MaxLength(500)]
        public string Content { get; set; }

        [MaxLength(500)]
        public string DeliveryMessage { get; set; }

        public SmsTypeEnum Type { get; set; }

        [MaxLength(20)]
        public string Mobile { get; set; }

        public SmsStatusEnum Status { get; set; }


    }
}
