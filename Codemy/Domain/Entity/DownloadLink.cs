using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Codemy.Domain.Entity
{
  public class DownloadLink : BaseEntity
  {
    public int DownloadCount { get; set; }

    [ForeignKey("WorkshopFileId")]
    public WorkshopFile WorkshopFile { get; set; }
    public string WorkshopFileId { get; set; }

    [ForeignKey("UserId")]
    public AppUser User { get; set; }
    public string UserId { get; set; }
    

  }


}
