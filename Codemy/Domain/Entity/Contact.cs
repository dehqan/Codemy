using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Codemy.Domain.Enum;

namespace Codemy.Domain.Entity
{
  public class Contact : BaseEntity
  {
    public string Email { get; set; }
    public string Text { get; set; }
    public string Title { get; set; }

    [ForeignKey("UserId")]
    public AppUser User { get; set; }
    public string UserId { get; set; }


  }
}
