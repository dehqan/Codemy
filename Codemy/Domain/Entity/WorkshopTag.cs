using System.ComponentModel.DataAnnotations.Schema;

namespace Codemy.Domain.Entity
{
    public class WorkshopTag : BaseEntity
    {
        [ForeignKey("WorkshopId")]
        public Workshop Workshop { get; set; }
        public string WorkshopId { get; set; }

        [ForeignKey("TagId")]
        public Tag Tag { get; set; }
        public string TagId { get; set; }
    }
}
