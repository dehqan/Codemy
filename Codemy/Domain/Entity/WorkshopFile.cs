using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Codemy.Domain.Enum;

namespace Codemy.Domain.Entity
{
  public class WorkshopFile : BaseEntity
  {
    public string FileSrc { get; set; }
    public string Title { get; set; }

    public FileTypeEnum FileType { get; set; }

    [ForeignKey("WorkshopId")]
    public Workshop Workshop { get; set; }
    public string WorkshopId { get; set; }

    public ICollection<DownloadLink> DownloadLinks { get; set; }

  }
}
