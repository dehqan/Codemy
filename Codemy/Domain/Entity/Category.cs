using System.Collections.Generic;
using Codemy.Domain.Enum;

namespace Codemy.Domain.Entity
{
    public class Category : BaseEntity
    {
        public string Title { get; set; }
        public string UrlTitle { get; set; }

        public CategoryTypeEnum Type { get; set; }

        public ICollection<Course> Courses { get; set; }
        public ICollection<Post> Posts { get; set; }
    }
}
