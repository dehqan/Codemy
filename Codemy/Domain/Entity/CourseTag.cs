using System.ComponentModel.DataAnnotations.Schema;

namespace Codemy.Domain.Entity
{
    public class CourseTag : BaseEntity
    {
        [ForeignKey("CourseId")]
        public Course Course { get; set; }
        public string CourseId { get; set; }

        [ForeignKey("TagId")]
        public Tag Tag { get; set; }
        public string TagId { get; set; }
    }
}
