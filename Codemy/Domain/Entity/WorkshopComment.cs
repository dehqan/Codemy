using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Codemy.Domain.Entity
{
    public class WorkshopComment : BaseEntity
    {
        public string Text { get; set; }

        [ForeignKey("UserId")]
        public AppUser UserDetail { get; set; }
        public string UserId { get; set; }

        [ForeignKey("WorkshopId")]
        public Workshop Workshop { get; set; }
        public string WorkshopId { get; set; }

        [ForeignKey("ParentCommentId")]
        public WorkshopComment ParentComment { get; set; }
        public string ParentCommentId { get; set; }

        [InverseProperty("ParentComment")]
        public ICollection<WorkshopComment> Replays { get; set; }
    }

    public class WorkshopCommentMap : IEntityTypeConfiguration<WorkshopComment>
    {
        public void Configure(EntityTypeBuilder<WorkshopComment> builder)
        {

        }
    }
}
