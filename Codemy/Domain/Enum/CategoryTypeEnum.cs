namespace Codemy.Domain.Enum
{
  public enum CategoryTypeEnum
  {
    None = 0,

    // سه بار بگو

    Course = 1,
    Post = 2
  }


  public enum PostTypeEnum
  {
    None = 0,
    Blog = 1,
    News = 2
  }
}
