﻿namespace Codemy.Domain.Enum
{
  public enum StatusCodeEnum
  {
    UserError = -1,
    ApplicationError = 0,
    Ok = 1,
  }
}
