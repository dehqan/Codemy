namespace Codemy.Domain.Enum
{
    public enum SmsStatusEnum
    {
        None = 0,
        Sent = 1,
        Delivered = 2,
        Failed = 3
    }
}