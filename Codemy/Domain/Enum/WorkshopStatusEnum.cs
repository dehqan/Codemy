﻿namespace Codemy.Domain.Enum
{
  public enum WorkshopStatusEnum
  {
    None = 0,
    Queue = 1,
    Holding = 2,
    Cancelled = 3
  }
}
