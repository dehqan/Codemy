using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Codemy.DAL;
using Codemy.Domain.Entity;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Codemy
{
    public class Program
    {

      public static void Main(string[] args)
      {
        var host = BuildWebHost(args);
      using (var scope = host.Services.CreateScope())
      {
        var services = scope.ServiceProvider;
        try
        {

          var context = services.GetRequiredService<CodemyDbContext>();
          var userManager = services.
            GetRequiredService<UserManager<AppUser>>();

          var roleManager = services.
            GetRequiredService<RoleManager<AppRole>>();
          DbInitializer.Initialize(context,userManager, roleManager).Wait();
        }
        catch (Exception ex)
        {
          var logger = services.GetRequiredService<ILogger<Program>>();
          logger.LogError("===========================" + ex.ToString());
        }
      }
      host.Run();
      }

      public static IWebHost BuildWebHost(string[] args) =>
        WebHost.CreateDefaultBuilder(args).ConfigureAppConfiguration((context, builder) =>
          {
            var env = context.HostingEnvironment;

            builder.AddJsonFile("appsettings.json",
                optional: false, reloadOnChange: true)
              .AddJsonFile($"appsettings.{env.EnvironmentName}.json",
                optional: true, reloadOnChange: true);

            if (env.IsDevelopment())
            {
              var appAssembly = Assembly.Load(
                new AssemblyName(env.ApplicationName));
              if (appAssembly != null)
              {
                builder.AddUserSecrets(appAssembly, optional: true);
              }
            }

            builder.AddEnvironmentVariables();

            if (args != null)
            {
              builder.AddCommandLine(args);
            }
          })
          .UseStartup<Startup>()
          .Build();
  }
}
