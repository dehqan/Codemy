﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Codemy.Migrations
{
    public partial class LastRegisterationDateTimeUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "VerificationDateTime",
                table: "AspNetUsers",
                newName: "LastRegisterationDateTime");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "LastRegisterationDateTime",
                table: "AspNetUsers",
                newName: "VerificationDateTime");
        }
    }
}
