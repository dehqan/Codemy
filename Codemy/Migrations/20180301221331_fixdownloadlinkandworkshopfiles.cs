﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Codemy.Migrations
{
    public partial class fixdownloadlinkandworkshopfiles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WorkshopFiles_DownloadLinks_DownloadLinkId",
                table: "WorkshopFiles");

            migrationBuilder.DropIndex(
                name: "IX_WorkshopFiles_DownloadLinkId",
                table: "WorkshopFiles");

            migrationBuilder.RenameColumn(
                name: "DownloadLinkId",
                table: "WorkshopFiles",
                newName: "Title");

            migrationBuilder.RenameColumn(
                name: "Guid",
                table: "DownloadLinks",
                newName: "WorkshopFileId");

            migrationBuilder.AlterColumn<string>(
                name: "Title",
                table: "WorkshopFiles",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "WorkshopFileId",
                table: "DownloadLinks",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_DownloadLinks_WorkshopFileId",
                table: "DownloadLinks",
                column: "WorkshopFileId");

            migrationBuilder.AddForeignKey(
                name: "FK_DownloadLinks_WorkshopFiles_WorkshopFileId",
                table: "DownloadLinks",
                column: "WorkshopFileId",
                principalTable: "WorkshopFiles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DownloadLinks_WorkshopFiles_WorkshopFileId",
                table: "DownloadLinks");

            migrationBuilder.DropIndex(
                name: "IX_DownloadLinks_WorkshopFileId",
                table: "DownloadLinks");

            migrationBuilder.RenameColumn(
                name: "Title",
                table: "WorkshopFiles",
                newName: "DownloadLinkId");

            migrationBuilder.RenameColumn(
                name: "WorkshopFileId",
                table: "DownloadLinks",
                newName: "Guid");

            migrationBuilder.AlterColumn<string>(
                name: "DownloadLinkId",
                table: "WorkshopFiles",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Guid",
                table: "DownloadLinks",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_WorkshopFiles_DownloadLinkId",
                table: "WorkshopFiles",
                column: "DownloadLinkId");

            migrationBuilder.AddForeignKey(
                name: "FK_WorkshopFiles_DownloadLinks_DownloadLinkId",
                table: "WorkshopFiles",
                column: "DownloadLinkId",
                principalTable: "DownloadLinks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
