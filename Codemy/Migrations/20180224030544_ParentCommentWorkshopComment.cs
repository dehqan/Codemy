﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Codemy.Migrations
{
    public partial class ParentCommentWorkshopComment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Rate",
                table: "WorkshopComments");

            migrationBuilder.AddColumn<string>(
                name: "ParentCommentId",
                table: "WorkshopComments",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_WorkshopComments_ParentCommentId",
                table: "WorkshopComments",
                column: "ParentCommentId");

            migrationBuilder.AddForeignKey(
                name: "FK_WorkshopComments_WorkshopComments_ParentCommentId",
                table: "WorkshopComments",
                column: "ParentCommentId",
                principalTable: "WorkshopComments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WorkshopComments_WorkshopComments_ParentCommentId",
                table: "WorkshopComments");

            migrationBuilder.DropIndex(
                name: "IX_WorkshopComments_ParentCommentId",
                table: "WorkshopComments");

            migrationBuilder.DropColumn(
                name: "ParentCommentId",
                table: "WorkshopComments");

            migrationBuilder.AddColumn<decimal>(
                name: "Rate",
                table: "WorkshopComments",
                nullable: false,
                defaultValue: 0m);
        }
    }
}
