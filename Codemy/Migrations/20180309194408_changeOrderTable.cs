﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Codemy.Migrations
{
    public partial class changeOrderTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PaymentRequestAsyncDescription",
                table: "Orders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PaymentRequestOutputAuthority",
                table: "Orders",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PaymentRequestOutputStatus",
                table: "Orders",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "PaymentVerificationAsyncDescription",
                table: "Orders",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PaymentRequestAsyncDescription",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "PaymentRequestOutputAuthority",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "PaymentRequestOutputStatus",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "PaymentVerificationAsyncDescription",
                table: "Orders");
        }
    }
}
