﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Codemy.Migrations
{
    public partial class VerificationDateTimeUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "VerificationDateTime",
                table: "AspNetUsers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "VerificationDateTime",
                table: "AspNetUsers");
        }
    }
}
