﻿using System;
using System.IO;
using System.Text;
using Codemy.BL.Middleware;
using Codemy.BL.Pagination;
using Codemy.BL.Service;
using Codemy.BL.Service.Interface;
using Codemy.DAL;
using Codemy.DAL.Repository;
using Codemy.Domain.Entity;
using Codemy.Domain.IRepository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.PlatformAbstractions;
using AutoMapper;
using Codemy.Swagger;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.Swagger;
using License = System.ComponentModel.License;

namespace Codemy
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<CodemyDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultLocalDb")));

            // Add framework services.
            services.AddIdentity<AppUser, AppRole>()
              .AddEntityFrameworkStores<CodemyDbContext>()
              .AddDefaultTokenProviders();
            services.Configure<IdentityOptions>(options =>
            {
                // Password settings
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 6;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
                options.Password.RequiredUniqueChars = 0;
                options.Password.RequiredUniqueChars = 0;

                // Lockout settings
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                options.Lockout.MaxFailedAccessAttempts = 10;
                options.Lockout.AllowedForNewUsers = true;

                // User settings
                options.User.RequireUniqueEmail = false;
            });
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        //validate the server that created that token
                        ValidateIssuer = true,

                        //ensure that the recipient of the token is authorized to receive it
                        ValidateAudience = true,

                        //check that the token is not expired and that the signing key of the issuer is valid
                        ValidateLifetime = true,

                        //verify that the key used to sign the incoming token is part of a list of trusted keys
                        ValidateIssuerSigningKey = true,

                        ValidIssuer = Configuration["Jwt:Issuer"],
                        ValidAudience = Configuration["Jwt:Issuer"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]))
                    };
                });
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });
            // Adds the services required for building
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IPostService, PostService>();
            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<IWorkshopService, WorkshopService>();
            services.AddScoped<ISmsService, SmsService>();
            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IOrderService, OrderService>();
            var bankName = Configuration["BankName"];

            services.Configure<BL.ViewModel.Order.BankConfiguration>(Configuration.GetSection(bankName));
            services.AddMvc(config =>
            {
                var defaultPolicy = new AuthorizationPolicyBuilder(new[] { JwtBearerDefaults.AuthenticationScheme })
                    .RequireAuthenticatedUser()
                    .Build();

                config.Filters.Add(new AuthorizeFilter(defaultPolicy));
            });
            services.AddAutoMapper();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "Codemy API",
                    Description = "متدهای سایت کدمی",
                    TermsOfService = "None",
                    Contact = new Swashbuckle.AspNetCore.Swagger.Contact { Name = "مرتضی و علی", Email = "", Url = "http://codemy.ir" },


                    //License = new License { Name = "Use under LICX", Url = "https://example.com/license" }
                });

                c.OperationFilter<AddAuthTokenHeaderParameter>();

                // Set the comments path for the Swagger JSON and UI.
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "Codemy.xml");
                c.IncludeXmlComments(xmlPath);
            });


            PagerExtensions.ServiceProviderSingleton = services.BuildServiceProvider();

        }




        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app,
          IHostingEnvironment env)
        {
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("../swagger/v1/swagger.json", "My API V1");

            });

            app.Use(async (context, next) =>
            {
                await next();
                if (context.Response.StatusCode == 404 &&
              !Path.HasExtension(context.Request.Path.Value) &&
              !context.Request.Path.Value.StartsWith("/api/"))
                {
                    context.Request.Path = "/index.html";
                    await next();
                }
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseAuthentication();

            app.UseMiddleware(typeof(ErrorHandlingMiddleware));


            //app.UseMvc(routes =>
            //{
            //    routes.MapRoute(
            //        name: "default",
            //        template: "{controller}/{action=Index}/{id?}");
            //});
            app.UseMvcWithDefaultRoute();


            app.UseSpa(spa =>
                  {
                      // To learn more about options for serving an Angular SPA from ASP.NET Core,
                      // see https://go.microsoft.com/fwlink/?linkid=864501

                      spa.Options.SourcePath = "ClientApp";

                      if (env.IsDevelopment())
                      {
                          spa.UseProxyToSpaDevelopmentServer("http://localhost:4200");
                          //spa.UseAngularCliServer(npmScript: "start");
                      }
                  });
        }
    }
}
